import 'dart:convert';

import 'package:beaconhill/model/newsletter_model.dart';
import 'package:http/http.dart' as http;
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:beaconhill/model/calendar_model.dart';
import 'package:beaconhill/model/carousel_model.dart';
import 'package:beaconhill/model/contact_model.dart';
import 'package:beaconhill/model/documents_model.dart';
import 'package:beaconhill/model/news_model.dart';
import 'package:beaconhill/model/notification_model.dart';
import 'package:beaconhill/model/secure_storage.dart';
import 'package:beaconhill/model/singleton.dart';
import 'package:beaconhill/utils/constants.dart';
import 'package:beaconhill/model/submit_request_model.dart';

class ApiManager {
  var _str = SecureStorage();

  Future<NewsModel> getNews() async {
    final response =
        await http.get(Uri.parse('$apiURL/wp-json/moko/v1/announcements'));
    if (response.statusCode == 200) {
      return NewsModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Failed to load album');
    }
  }

  Future<NotificationModel> getNotification() async {
    Map<String, String> httpHeader = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
      'x-api-key': apiKey
    };
    final response = await http.get(Uri.parse('$mokoURL/notifications'),
        headers: httpHeader);
    if (response.statusCode == 200) {
      return NotificationModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Failed to load album');
    }
  }

  Future<ContactModel> getContacts() async {
    final response =
        await http.get(Uri.parse('$apiURL/wp-json/moko/v1/contact_numbers'));
    if (response.statusCode == 200) {
      return ContactModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Failed to load album');
    }
  }

  Future<CarouselModel> getImage() async {
    final response =
        await http.get(Uri.parse('$apiURL/wp-json/moko/v1/carouselimg'));
    if (response.statusCode == 200) {
      return CarouselModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Failed to load album');
    }
  }

  Future<DocumentsModel> getDocuments() async {
    final response =
        await http.get(Uri.parse('$apiURL/wp-json/moko/v1/documents'));
    if (response.statusCode == 200) {
      return DocumentsModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Failed to load album');
    }
  }

  Future<SubmitARequestModel> submitRequest() async {
    var url = '$mokoURL/submit-request';
    Map<String, String> httpHeader = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
      'x-api-key': apiKey
    };
    var data = Singleton();
    print(data.userData['imagesData'].length);
    var postData = {
      'name': data.userData['name'],
      'description': data.userData['description'],
      'email': data.userData['email'],
      'phone': data.userData['phone'],
      'images': data.userData['imagesData'],
      'latitude': data.userData['location']['latitude'],
      'longitude': data.userData['location']['longitude']
    };
    var body = json.encode(postData);
    var response =
        await http.post(Uri.parse(url), headers: httpHeader, body: body);
    return SubmitARequestModel.fromJson(jsonDecode(response.body));
  }

  Future<CalendarModel> getCalendar() async {
    final startFrom = DateTime.now().subtract(Duration(days: 90));
    final endTo = DateTime.now().add(Duration(days: 90));
    final response = await http.get(Uri.parse(
        '$apiURL/wp-json/tribe/events/v1/events?start_date=$startFrom&end_date=$endTo&page=1&per_page=1000'));
    if (response.statusCode == 200) {
      return CalendarModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Failed to load album');
    }
  }

  Future<NewsletterModel> getNewsletter() async {
    final response =
        await http.get(Uri.parse('$apiURL/wp-json/moko/v1/newsletters'));
    if (response.statusCode == 200) {
      return NewsletterModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Failed to load album');
    }
  }

  void syncNotificationTags() async {
    try {
      final STORAGEKEY = 'notificationTagBeacon';
      List<dynamic> defaultTags = [
        {
          'id': 1,
          'tag': 'community_events',
          'name': 'Association/Community Events',
          'default': true,
          'group': 'Notification Tags'
        },
        {
          'id': 2,
          'tag': 'board_meetings',
          'name': 'Committee/Board Meetings',
          'default': true,
          'group': 'Notification Tags'
        },
        {
          'id': 3,
          'tag': 'emergency_alerts',
          'name': 'Emergency Alerts',
          'default': true,
          'group': 'Notification Tags'
        },
        {
          'id': 4,
          'tag': 'snow_removal',
          'name': 'Snow Removal',
          'default': true,
          'group': 'Notification Tags'
        },
        {
          'id': 5,
          'tag': 'trash_recycling',
          'name': 'Trash & Recycling',
          'default': true,
          'group': 'Notification Tags'
        },
        {
          'id': 6,
          'tag': '',
          'name': 'Privacy Policy',
          'group': 'Legal',
        },
        {
          'id': 7,
          'tag': '',
          'name': 'Terms of Use',
          'group': 'Legal',
        }
      ];
      var data = await _str.readSecureData(STORAGEKEY);

      if (data != null) {
        var parsedTag = jsonDecode(data);
        for (var i = 0; i < defaultTags.length; i++) {
          if (parsedTag[i]['group'] != 'Legal') {
            if (defaultTags[i]['id'] == parsedTag[i]['id']) {
              if (parsedTag[i]['default']) {
                OneSignal.shared.sendTag(
                    parsedTag[i]['tag'].toString(), parsedTag[i]['tag']);
              } else {
                OneSignal.shared.deleteTag(parsedTag[i]['tag'].toString());
              }
            }
          }
        }
      } else {
        _str.writeSecureData(STORAGEKEY, json.encode(defaultTags));
      }
    } catch (e) {
      print(e);
    }
  }
}
