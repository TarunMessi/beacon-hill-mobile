// import 'package:beaconhill/screens/calendar/calendar_screen.dart';
// import 'package:beaconhill/screens/contactInfo/contact_screen.dart';
// import 'package:beaconhill/screens/documents/documents_screen.dart';
// import 'package:beaconhill/screens/more/more_screen.dart';



import 'package:beaconhill/screens/contactInfo/contact_screen.dart';
import 'package:beaconhill/screens/more/more_screen.dart';
import 'package:beaconhill/screens/newsletter/newsletter_screen.dart';

import 'package:beaconhill/screens/calendar/calendar_screen.dart';
import 'package:beaconhill/screens/documents/documents_screen.dart';
import 'package:beaconhill/screens/home/home_screen.dart';
import 'package:beaconhill/screens/news/news_screen.dart';
import 'package:beaconhill/screens/notifications/notifications_screen.dart';
import 'package:beaconhill/screens/settings/settings_screen.dart';
import 'package:beaconhill/screens/submitRequest/submit_request_screen.dart';
import 'package:flutter/cupertino.dart';
// import 'package:beaconhill/screens/notifications/notifications_screen.dart';
// import 'package:beaconhill/screens/settings/settings_screen.dart';
// import 'package:beaconhill/screens/submitRequest/submit_request_screen.dart';

final Map<String, WidgetBuilder> routes = {
  HomeScreen.routeName: (context) => HomeScreen(),
  NewsScreen.routeName: (context) => NewsScreen(),
  NotificationScreen.routeName: (context) => NotificationScreen(),
  CalendarScreen.routeName: (context) => CalendarScreen(),
  DocumentsScreen.routeName: (context) => DocumentsScreen(),
  NewsLetterScreen.routeName: (context) => NewsLetterScreen(),
  MoreScreen.routeName: (context) => MoreScreen(),
  ContactScreen.routeName: (context) => ContactScreen(),
  SettingsScreen.routeName: (context) => SettingsScreen(),
  SubmitARequestScreen.routeName: (context) => SubmitARequestScreen(),
  //   NotificationScreen.routeName: (context) => NotificationScreen(),
//   ContactScreen.routeName: (context) => ContactScreen(),
//   SettingsScreen.routeName: (context) => SettingsScreen(),
//   SubmitARequestScreen.routeName: (context) => SubmitARequestScreen(),
//   DocumentsScreen.routeName: (context) => DocumentsScreen(),
//   CalendarScreen.routeName: (context) => CalendarScreen(),
//   NewsLetterScreen.routeName: (context) => NewsLetterScreen()
};
