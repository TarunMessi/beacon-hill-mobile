import 'package:flutter/material.dart';
import 'package:beaconhill/screens/settings/components/body.dart';

class SettingsScreen extends StatelessWidget {
  static String routeName = '/settings_screen';
  const SettingsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Settings'),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: Body(),
    );
  }
}
