import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:grouped_list/grouped_list.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:beaconhill/model/secure_storage.dart';
import 'package:beaconhill/screens/settings/components/web_view.dart';

class Body extends StatefulWidget {
  const Body({Key? key}) : super(key: key);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  var _str = SecureStorage();
  final STORAGEKEY = 'notificationTagBeacon';

  List<dynamic> defaultTags = [
    {
      'id': 1,
      'tag': 'community_events',
      'name': 'Association/Community Events',
      'default': true,
      'group': 'Notification Tags'
    },
    {
      'id': 2,
      'tag': 'board_meetings',
      'name': 'Committee/Board Meetings',
      'default': true,
      'group': 'Notification Tags'
    },
    {
      'id': 3,
      'tag': 'emergency_alerts',
      'name': 'Emergency Alerts',
      'default': true,
      'group': 'Notification Tags'
    },
    {
      'id': 4,
      'tag': 'snow_removal',
      'name': 'Snow Removal',
      'default': true,
      'group': 'Notification Tags'
    },
    {
      'id': 5,
      'tag': 'trash_recycling',
      'name': 'Trash & Recycling',
      'default': true,
      'group': 'Notification Tags'
    },
    {
      'id': 6,
      'tag': '',
      'name': 'Privacy Policy',
      'group': 'Legal',
    },
    {
      'id': 7,
      'tag': '',
      'name': 'Terms of Use',
      'group': 'Legal',
    }
  ];

  @override
  void initState() {
    // ignore: todo
    // TODO: implement initState
    super.initState();
    initiateTags();
  }

  initiateTags() async {
    var data = await _str.readSecureData(STORAGEKEY);
    if (data != null) {
      List<dynamic> parsedData = jsonDecode(data);
      setState(() {
        defaultTags = parsedData;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return GroupedListView<dynamic, String>(
      elements: defaultTags,
      groupBy: (element) => element['group'],
      groupComparator: (value1, value2) => value2.compareTo(value1),
      itemComparator: (item1, item2) => item1['id'].compareTo(item2['id']),
      order: GroupedListOrder.ASC,
      useStickyGroupSeparators: true,
      groupSeparatorBuilder: (String value) => Padding(
        padding: const EdgeInsets.all(15.0),
        child: Text(
          value,
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
        ),
      ),
      itemBuilder: (c, element) {
        if (element['group'] == 'Legal') {
          return Card(
            elevation: 8.0,
            margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 0.0),
            child: Container(
              child: ListTile(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => WebPage(
                                title: element['name'],
                              )));
                },
                contentPadding:
                    EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                title: Text(element['name']),
                trailing: Icon(MaterialIcons.keyboard_arrow_right),
              ),
            ),
          );
        } else {
          return Card(
            elevation: 8.0,
            margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 0.0),
            child: Container(
              child: ListTile(
                contentPadding:
                    EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                title: Text(element['name']),
                trailing: customSwitch(element),
              ),
            ),
          );
        }
      },
    );
  }

  Widget customSwitch(element) {
    return CupertinoSwitch(
      onChanged: (val) {
        var index = element['id'];
        var selectedIndex = defaultTags[index - 1];
        selectedIndex['default'] = val;
        var copyTags = defaultTags;
        copyTags.removeAt(index - 1);
        copyTags.insert(index - 1, selectedIndex);
        if (val) {
          OneSignal.shared
              .sendTag(selectedIndex['tag'].toString(), selectedIndex['tag']);
        } else {
          OneSignal.shared.deleteTag(selectedIndex['tag'].toString());
        }
        setState(() {
          defaultTags = copyTags;
        });
        _str.writeSecureData(STORAGEKEY, json.encode(copyTags));
      },
      value: element['default'],
    );
  }
}
