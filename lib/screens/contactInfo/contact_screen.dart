import 'package:flutter/material.dart';
import 'package:beaconhill/screens/contactInfo/components/body.dart';

class ContactScreen extends StatelessWidget {
  static String routeName = '/contact_screen';
  const ContactScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Contact Numbers'),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: Body(),
    );
  }
}
