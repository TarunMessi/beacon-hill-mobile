import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:beaconhill/model/contact_model.dart';
import 'package:beaconhill/services/api_manager.dart';
import 'package:grouped_list/grouped_list.dart';

class Body extends StatefulWidget {
  const Body({Key? key}) : super(key: key);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  late Future<ContactModel> _contactModel;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _contactModel = ApiManager().getContacts();
  }

  Widget groupedView(AsyncSnapshot<ContactModel> snapshot) {
    if (snapshot.data!.contactNumbers.isNotEmpty) {
      return GroupedListView<dynamic, String>(
        elements: snapshot.data!.contactNumbers,
        groupBy: (element) => element.title,
        itemComparator: (item1, item2) => item1.name.compareTo(item2.name),
        order: GroupedListOrder.ASC,
        useStickyGroupSeparators: true,
        groupSeparatorBuilder: (String value) => Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            value,
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          ),
        ),
        itemBuilder: (c, element) {
          return Card(
            elevation: 8.0,
            margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
            child: Container(
              child: ListTile(
                contentPadding:
                    EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                // leading: Icon(Icons.account_circle),
                title: Text(element.name),
                trailing: Icon(MaterialIcons.phone),
                onTap: () async {
                  if (element.number != null) {
                    String tel = 'tel:${element.number}';
                    if (await canLaunch(tel)) {
                      await launch(tel);
                    } else {
                      throw 'Could not launch $tel';
                    }
                  }
                },
              ),
            ),
          );
        },
      );
    } else {
      return Container(
        child: Center(
          child: Text('No more contacts'),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<ContactModel>(
      future: _contactModel,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          if (snapshot.hasData) {
            return this.groupedView(snapshot);
          } else {
            return Center(
              child: Text('No more contacts'),
            );
          }
        }
        return Center(child: CircularProgressIndicator());
      },
    );
  }
}
