import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:beaconhill/model/carousel_model.dart';
import 'package:beaconhill/model/secure_storage.dart';
import 'package:beaconhill/services/api_manager.dart';
import 'package:beaconhill/utils/constants.dart';
import 'package:beaconhill/utils/menu_widget.dart';
// import 'package:focus_detector/focus_detector.dart';

class Body extends StatefulWidget {
  const Body({Key? key}) : super(key: key);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  late CarouselModel _carousel;
  String imageURL = '';
  int badgeCount = 0;
  String STORAGE_KEY_PREVIOUS_COUNT = 'previousCount';
  var _str = SecureStorage();

  @override
  void didChangeDependencies() async {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    _carousel = await ApiManager().getImage();
    initialLoad();

    setState(() {
      imageURL = _carousel.carouselImages[0].toString();
    });
  }

  void screenFocused() async {
    var isFromAlerts = await _str.readSecureData('fromAlerts');
    if (isFromAlerts != null) {
      _str.deleteSecureData('fromAlerts');
      var notification = await ApiManager().getNotification();
      int _notificationCount = 0;
      int current_count = notification.data.length;
      var notificationCount_prev =
          await _str.readSecureData(STORAGE_KEY_PREVIOUS_COUNT);
      if (notificationCount_prev != null) {
        int count = current_count - int.parse(notificationCount_prev);
        _notificationCount = count > 0 ? count : 0;
      } else {
        _str.writeSecureData(
            STORAGE_KEY_PREVIOUS_COUNT, json.encode(current_count));
      }
      setState(() {
        badgeCount = _notificationCount;
      });
    }
  }

  void initialLoad() async {
    var notification = await ApiManager().getNotification();
    int _notificationCount = 0;
    int current_count = notification.data.length;
    var notificationCount_prev =
        await _str.readSecureData(STORAGE_KEY_PREVIOUS_COUNT);
    if (notificationCount_prev != null) {
      int count = current_count - int.parse(notificationCount_prev);
      _notificationCount = count > 0 ? count : 0;
    } else {
      _str.writeSecureData(
          STORAGE_KEY_PREVIOUS_COUNT, json.encode(current_count));
    }
    setState(() {
      badgeCount = _notificationCount;
    });
  }

  @override
  Widget build(BuildContext context) {
    // return FocusDetector(
    //   onFocusGained: screenFocused,
    //   child: Column(
      return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Expanded(
            flex: 1,
            child: Container(
                color: primaryColor,
                child: Image.asset('images/Logo.jpeg', fit: BoxFit.contain)),
          ),
          Expanded(
            flex: 1,
            child: Container(
                color: Colors.white,
                child: imageURL == ''
                    ? Image.asset('images/placeHolder.jpg', fit: BoxFit.contain)
                    : Image.network(
                        imageURL,
                        fit: BoxFit.fill,
                      )),
          ),
          Expanded(
            flex: 1,
            child: Column(
              children: [
                Row(
                  children: [
                    MenuWidget(
                      iconName: Ionicons.newspaper_outline,
                      displayName: 'News',
                      border: true,
                      routeName: '/news_screen',
                    ),
                    MenuWidget(
                        iconName: SimpleLineIcons.flag,
                        displayName: 'Alerts',
                        border: true,
                        routeName: '/notification_screen',
                        badgeNo: badgeCount),
                    MenuWidget(
                      iconName: FontAwesome.calendar,
                      displayName: 'Calendar',
                      border: true,
                      routeName: '/calendar_screen',
                    ),
                    MenuWidget(
                      iconName: FontAwesome.folder_open_o,
                      displayName: 'Documents',
                      border: true,
                      routeName: '/documents_screen',
                    ),
                  ],
                ),
                Row(
                  children: [
                    MenuWidget(
                      iconName: MaterialIcons.content_paste,
                      displayName: 'Newsletter\n',
                      border: true,
                      routeName: '/newsletter_screen',
                    ),
                    MenuWidget(
                      iconName: Ionicons.location_sharp,
                      displayName: 'Submit A\nRequest',
                      border: true,
                      routeName: '/submit_request_screen',
                    ),
                    MenuWidget(
                      iconName: MaterialIcons.payment,
                      displayName: 'Pay\nDues',
                      border: true,
                      routeName: 'pay_dues',
                    ),
                    MenuWidget(
                      iconName: MaterialIcons.more_horiz,
                      displayName: 'More\n',
                      border: true,
                      routeName: '/more_screen',
                    ),
                  ],
                )
              ],
            ),
          )
        ],
      );
  }
}
