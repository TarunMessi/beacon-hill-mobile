import 'package:flutter/material.dart';
import 'package:beaconhill/screens/documents/components/body.dart';

class DocumentsScreen extends StatelessWidget {
  static String routeName = '/documents_screen';
  const DocumentsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Documents'),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: Body(),
    );
  }
}
