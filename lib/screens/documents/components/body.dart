import 'package:flutter/material.dart';
import 'package:grouped_list/grouped_list.dart';
import 'package:beaconhill/model/documents_model.dart';
import 'package:beaconhill/screens/documents/components/pdf_viewer.dart';
// import 'package:beaconhill/screens/documents/components/pdfViewer.dart';
import 'package:beaconhill/services/api_manager.dart';

class Body extends StatefulWidget {
  const Body({Key? key}) : super(key: key);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  late Future<DocumentsModel> _documentModel;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _documentModel = ApiManager().getDocuments();
  }

  Widget groupedView(AsyncSnapshot<DocumentsModel> snapshot) {
    if (snapshot.data!.documents.isNotEmpty) {
      return GroupedListView<dynamic, String>(
        elements: snapshot.data!.documents,
        groupBy: (element) => element.category,
        groupComparator: (value1, value2) => value2.compareTo(value1),
        // itemComparator: (item1, item2) => item1.title.compareTo(item2.title),
        // order: GroupedListOrder.ASC,
        useStickyGroupSeparators: true,
        groupSeparatorBuilder: (String value) => Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            value,
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          ),
        ),
        itemBuilder: (c, element) {
          return GestureDetector(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => PdfViewer(
                            title: element.title,
                            url: element.url,
                          )));
            },
            child: Card(
              elevation: 8.0,
              margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
              child: Container(
                child: ListTile(
                  contentPadding:
                      EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                  title: Text(element.title),
                  trailing: Icon(Icons.arrow_forward),
                ),
              ),
            ),
          );
        },
      );
    } else {
      return Container(
        child: Center(
          child: Text('No more documents'),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<DocumentsModel>(
      future: _documentModel,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          if (snapshot.hasData) {
            return this.groupedView(snapshot);
          } else {
            return Center(
              child: Text('No more documents'),
            );
          }
        }
        return Center(child: CircularProgressIndicator());
      },
    );
  }
}
