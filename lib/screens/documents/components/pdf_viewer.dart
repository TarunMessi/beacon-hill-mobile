import 'dart:io';
import 'dart:isolate';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:advance_pdf_viewer/advance_pdf_viewer.dart';
import 'package:flutter_downloader/flutter_downloader.dart';

class PdfViewer extends StatefulWidget {
  const PdfViewer({Key? key, this.title, this.url}) : super(key: key);
  final title;
  final url;
  @override
  _PdfViewerState createState() => _PdfViewerState();
}

class _PdfViewerState extends State<PdfViewer> {
  ReceivePort _port = ReceivePort();
  bool _isLoading = true;

  late PDFDocument document;
  @override
  void initState() {
    super.initState();
    loadDocument();
    IsolateNameServer.registerPortWithName(
        _port.sendPort, 'downloader_send_port');
    _port.listen((dynamic data) {
      String id = data[0];
      DownloadTaskStatus status = data[1];
      int progress = data[2];
      setState(() {});
    });

    FlutterDownloader.registerCallback(downloadCallback);
  }

  @override
  void dispose() {
    IsolateNameServer.removePortNameMapping('downloader_send_port');
    super.dispose();
  }

  static void downloadCallback(
      String id, DownloadTaskStatus status, int progress) {
    final SendPort send =
        IsolateNameServer.lookupPortByName('downloader_send_port')!;
    send.send([id, status, progress]);
  }

  loadDocument() async {
    document = await PDFDocument.fromURL(widget.url);
    if (mounted) {
      setState(() => _isLoading = false);
    }
  }

  downloadAndroid(cont) async {
    setState(() {
      _isLoading = true;
    });
    final externalDir = await getExternalStorageDirectory();
    final id = await FlutterDownloader.enqueue(
      url: widget.url,
      savedDir: externalDir!.path,
      fileName: '${widget.title}.pdf',
      showNotification: true,
      openFileFromNotification: true,
    );
    setState(() {
      _isLoading = false;
    });
    showAlertDialog(cont);
  }

  downloadIOS(cont) async {
    setState(() {
      _isLoading = true;
    });
    final externalDirIOS = await getApplicationDocumentsDirectory();
    final id = await FlutterDownloader.enqueue(
      url: widget.url,
      savedDir: externalDirIOS.path,
      fileName: '${widget.title}.pdf',
      showNotification: true,
      openFileFromNotification: true,
    );
    setState(() {
      _isLoading = false;
    });
    showAlertDialog(cont);
  }

  download(cont) async {
    final status = await Permission.storage.request();
    if (status.isGranted) {
      Platform.isIOS ? downloadIOS(cont) : downloadAndroid(cont);
    } else {
      print("Permission deined");
    }
  }

  showAlertDialog(BuildContext context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Success!"),
            content: Text("Downloaded Successfully."),
            actions: <Widget>[
              TextButton(
                child: Text("ok"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Documents'),
          centerTitle: true,
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => Navigator.of(context).pop(),
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(Entypo.download),
              onPressed: () => download(context),
            ),
          ],
        ),
        body: Center(
          child: _isLoading
              ? CircularProgressIndicator()
              : PDFViewer(
                  document: document,
                  zoomSteps: 1,
                  scrollDirection: Axis.vertical,
                ),
        ));
  }
}
