import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
// import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:html/dom.dart' as dom;

class DetailNews extends StatefulWidget {
  const DetailNews({Key? key, required this.title, required this.description})
      : super(key: key);
  final String title;
  final String description;

  @override
  _DetailNewsState createState() => _DetailNewsState();
}

class _DetailNewsState extends State<DetailNews> {
  // final flutterWebviewPlugin = new FlutterWebviewPlugin();
  var html = '';
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadData();
  }

  @override
  Widget build(BuildContext context) {
    // flutterWebviewPlugin.onUrlChanged.listen((String url) async {
    //   if (url.substring(0, 4) == 'http' ||
    //       url.substring(0, 3) == 'tel' ||
    //       url.substring(0, 6) == 'mailto' ||
    //       url.substring(0, 3) == 'geo' ||
    //       url.substring(0, 3) == 'sms') {
    //     if (await canLaunch(url)) {
    //       flutterWebviewPlugin.stopLoading();
    //       _launchURL(url);
    //     } else {
    //       throw 'Could not launch $url';
    //     }
    //   }
    // });
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
          centerTitle: true,
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
            child: Html(
                data: html,
                onLinkTap: (String? url,
                    RenderContext context,
                    Map<String, String> attributes,
                    dom.Element? element) async {
                  await launch(url!);
                }),
          ),
        ));
  }

  void _launchURL(url) async => await canLaunch(url)
      ? await launch(
          url,
          forceSafariVC: false,
          forceWebView: false,
        )
      : throw 'Could not launch $url';

  loadData() {
    if (Platform.isAndroid) {
      html = '''<style>html *
            {font-family: "LibreBaskerville-Regular" !important;}.normal-content section.moko,.normal-content>:not(section){padding:5px 20px}.normal-content header.moko{padding:5px 20px;background:#eee;border-top:solid 1px #ddd;border-bottom:solid 1px #ddd}.normal-content header.moko h2{margin:0 0 0 -10px;font-style:italic;font-size:21px}.normal-content section.moko h1{font-size:18px}.normal-content section.moko h2{font-size:16px}.normal-content section.moko h3{font-size:14px}.normal-content section.moko h1,.normal-content section.moko h2,.normal-content section.moko h3{margin:.5em 0}.normal-content section.moko p{font-size:14px;line-height:1.3}.normal-content section.moko blockquote{margin:0}.normal-content section.moko img{max-width:100%;margin:5px;border-radius:5px}.normal-content section.moko th{font-size:14px;word-break:break-word}.normal-content section.moko td{font-size:12px;padding-bottom:15px}</style><body><div id="announcements" class="normal-content">''';
    } else {
      html = '''<style>html *
            {font-family: "LibreBaskerville-Regular" !important;}.normal-content section.moko,.normal-content>:not(section){padding:5px 20px}.normal-content header.moko{padding:5px 20px;background:#eee;border-top:solid 1px #ddd;border-bottom:solid 1px #ddd}.normal-content header.moko h2{margin:0 0 0 -10px;font-style:italic;font-size:21px}.normal-content section.moko h1{font-size:18px}.normal-content section.moko h2{font-size:50px}.normal-content section.moko h3{font-size:14px}.normal-content section.moko h1,.normal-content section.moko h2,.normal-content section.moko h3{margin:.5em 0}.normal-content section.moko p{font-size:40px;line-height:1.3}.normal-content section.moko blockquote{margin:0}.normal-content section.moko img{max-width:100%;margin:5px;border-radius:5px}.normal-content section.moko th{font-size:14px;word-break:break-word}.normal-content section.moko td{font-size:12px;padding-bottom:15px}figcaption{font-size:35px;}</style><body><div id="announcements" class="normal-content">''';
    }
    html += '''<section class='moko'>''';
    html += '''<h2>${widget.title}</h2>''';
    html += '''${widget.description}''';
    html += '</section>';
    html += '</div></body>';
  }
}


// import 'package:flutter/material.dart';
// import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
// import 'package:url_launcher/url_launcher.dart';

// class DetailNews extends StatelessWidget {
//   DetailNews({Key? key, required this.title, required this.description})
//       : super(key: key);
//   final String title;
//   final String description;

//   final flutterWebviewPlugin = new FlutterWebviewPlugin();

//   @override
//   Widget build(BuildContext context) {
//     void _launchURL(url) async => await canLaunch(url)
//         ? await launch(
//             url,
//             forceSafariVC: false,
//             forceWebView: false,
//           )
//         : throw 'Could not launch $url';
//     flutterWebviewPlugin.onUrlChanged.listen((String url) async {
//       if (url.substring(0, 4) == 'http' ||
//           url.substring(0, 3) == 'tel' ||
//           url.substring(0, 6) == 'mailto' ||
//           url.substring(0, 3) == 'geo' ||
//           url.substring(0, 3) == 'sms') {
//         if (await canLaunch(url)) {
//           flutterWebviewPlugin.stopLoading();
//           _launchURL(url);
//         } else {
//           throw 'Could not launch $url';
//         }
//       }
//     });
//     return Scaffold(
//       appBar: AppBar(
//         title: Text(title),
//         centerTitle: true,
//         leading: IconButton(
//           icon: Icon(Icons.arrow_back, color: Colors.white),
//           onPressed: () => Navigator.of(context).pop(),
//         ),
//       ),
//       body: WebviewScaffold(
//         url: new Uri.dataFromString(description,
//                 mimeType: 'text/html', encoding: Encoding.getByName('utf-8'))
//             .toString(),
//         withZoom: true,
//         withJavascript: true,
//       ),
//     );
//   }

//   data() {
//     if (Platform.isAndroid) {
//       html = '''<style>html *
//             {font-family: "LibreBaskerville-Regular" !important;}.normal-content section.moko,.normal-content>:not(section){padding:5px 20px}.normal-content header.moko{padding:5px 20px;background:#eee;border-top:solid 1px #ddd;border-bottom:solid 1px #ddd}.normal-content header.moko h2{margin:0 0 0 -10px;font-style:italic;font-size:21px}.normal-content section.moko h1{font-size:18px}.normal-content section.moko h2{font-size:16px}.normal-content section.moko h3{font-size:14px}.normal-content section.moko h1,.normal-content section.moko h2,.normal-content section.moko h3{margin:.5em 0}.normal-content section.moko p{font-size:14px;line-height:1.3}.normal-content section.moko blockquote{margin:0}.normal-content section.moko img{max-width:100%;margin:5px;border-radius:5px}.normal-content section.moko th{font-size:14px;word-break:break-word}.normal-content section.moko td{font-size:12px;padding-bottom:15px}</style><body><div id="announcements" class="normal-content">''';
//     } else {
//       html = '''<style>html *
//             {font-family: "LibreBaskerville-Regular" !important;}.normal-content section.moko,.normal-content>:not(section){padding:5px 20px}.normal-content header.moko{padding:5px 20px;background:#eee;border-top:solid 1px #ddd;border-bottom:solid 1px #ddd}.normal-content header.moko h2{margin:0 0 0 -10px;font-style:italic;font-size:21px}.normal-content section.moko h1{font-size:18px}.normal-content section.moko h2{font-size:50px}.normal-content section.moko h3{font-size:14px}.normal-content section.moko h1,.normal-content section.moko h2,.normal-content section.moko h3{margin:.5em 0}.normal-content section.moko p{font-size:40px;line-height:1.3}.normal-content section.moko blockquote{margin:0}.normal-content section.moko img{max-width:100%;margin:5px;border-radius:5px}.normal-content section.moko th{font-size:14px;word-break:break-word}.normal-content section.moko td{font-size:12px;padding-bottom:15px}figcaption{font-size:35px;}</style><body><div id="announcements" class="normal-content">''';
//     }
//     html += '''<section class='moko'>''';
//     html += '''<h2>${widget.title}</h2>''';
//     html += '''${widget.description}''';
//     html += '</section>';
//     html += '</div></body>';
//   }
// }
