import 'package:flutter/material.dart';
import 'package:beaconhill/model/news_model.dart';
import 'package:beaconhill/services/api_manager.dart';
import 'package:grouped_list/grouped_list.dart';
import 'package:intl/intl.dart';

import 'detail_news.dart';

class Body extends StatefulWidget {
  const Body({Key? key}) : super(key: key);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  late Future<NewsModel> _newsModel;
  var dateFormat = DateFormat('M/dd');
  var groupFormat = DateFormat('yyyy/MM');

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _newsModel = ApiManager().getNews();
  }

  Widget headerTitle(value) {
    DateTime parseDate = new DateFormat("yyyy/MM").parse(value);
    var inputDate = DateTime.parse(parseDate.toString());
    var outputFormat = DateFormat('MMMM yyyy');
    var outputDate = outputFormat.format(inputDate);
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text(
        outputDate,
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
      ),
    );
  }

  Widget groupedList(snapshot) {
    if (snapshot.data!.announcements.isNotEmpty) {
      return GroupedListView<dynamic, String>(
        elements: snapshot.data!.announcements,
        groupBy: (element) => groupFormat.format(element.postDate).toString(),
        itemComparator: (item1, item2) => item1.id.compareTo(item2.id),
        order: GroupedListOrder.DESC,
        useStickyGroupSeparators: true,
        groupSeparatorBuilder: (String value) => this.headerTitle(value),
        itemBuilder: (c, element) {
          return Card(
            elevation: 8.0,
            margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
            child: Container(
              child: ListTile(
                contentPadding:
                    EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                leading: Wrap(
                  children: [
                    Text(dateFormat.format(element.postDate).toString())
                  ],
                ),
                title: Text(element.postTitle),
                trailing: Icon(Icons.arrow_forward),
                onTap: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => DetailNews(
                            title: element.postTitle,
                            description: element.postContent))),
              ),
            ),
          );
        },
      );
    } else {
      return Container(
        child: Center(
          child: Text('No more news'),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: FutureBuilder<NewsModel>(
        future: _newsModel,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasData) {
              return this.groupedList(snapshot);
            } else {
              return Center(
                child: Text('No more news'),
              );
            }
          }
          return Center(child: CircularProgressIndicator());
        },
      ),
    );
  }
}
