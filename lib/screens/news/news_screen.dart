import 'package:beaconhill/screens/news/component/body.dart';
import 'package:flutter/material.dart';

class NewsScreen extends StatelessWidget {
  static String routeName = '/news_screen';
  const NewsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('News'),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: Body(),
    );
  }
}
