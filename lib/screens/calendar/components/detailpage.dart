import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:html/dom.dart' as dom;
import 'package:beaconhill/model/calendar_model.dart';
import 'package:beaconhill/utils/constants.dart';

class DetailPage extends StatefulWidget {
  Event appointments;
  DetailPage(this.appointments);
  @override
  _DetailPageState createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  var html = '''<style>
        .normal-content section.moko,.normal-content>:not(section){
            padding:5px 20px
        }
        .normal-content header.moko{
            padding:5px 20px;
            background:#eee;
            border-top:solid 1px #ddd;
            border-bottom:solid 1px #ddd
        }
        .normal-content header.moko h2{
            margin:0 0 0 -10px;
            font-style:italic;
            font-size:${(Platform.isIOS) ? '55px' : '21px'};
        }
        .normal-content section.moko h1{
            font-size:${(Platform.isIOS) ? '50px' : '18px'};
        }
        .normal-content section.moko h2{
            font-size:${(Platform.isIOS) ? '45px' : '16px'};
        }
        .normal-content section.moko h3{
            font-size:${(Platform.isIOS) ? '40px' : '14px'};
        }
        .normal-content section.moko h1,.normal-content section.moko h2,.normal-content section.moko h3{
            margin:.5em 0
        }
        .normal-content section.moko p{
            font-size:${(Platform.isIOS) ? '40px' : '14px'};
            line-height:1.3
        }
        .normal-content section.moko blockquote {
            margin:0
        }
        .normal-content section.moko img{
            max-width:100%;
            margin:5px;
            border-radius:5px
        }
        .normal-content section.moko th{
            font-size:${(Platform.isIOS) ? '40px' : '14px'};
            word-break:break-word
        }
        .normal-content section.moko td{
            font-size:${(Platform.isIOS) ? '35px' : '12px'};
            padding-bottom:15px
        }
        </style><body>''';

  @override
  Widget build(BuildContext context) {
    html +=
        """<div id="events" class="normal-content"><section class='moko'>""";
    html += "<h2>${widget.appointments.title}</h2>`";
    if (widget.appointments.organizer.length > 0) {
      if (Platform.isIOS) {
        html +=
            """<span style="font-style:italic;color:'grey';font-size:35px;">${widget.appointments.organizer[0].organizer}</span>""";
      } else {
        html +=
            """<span style="font-style:italic;color:'grey';">${widget.appointments.organizer[0].organizer}</span>""";
      }
    }
    html += widget.appointments.description;
    if (widget.appointments.venue.length > 0) {
      if (Platform.isIOS) {
        html += """<div style = "font-size:35px;"><h2>Location: </h2>""";
      } else {
        html += """<div><h2>Location: </h2>""";
      }
    }
    html += widget.appointments.venue.length > 0
        ? widget.appointments.venue['venue'] != null
            ? widget.appointments.venue['venue']
            : ''
        : '' + '<br>';
    html += widget.appointments.venue.length > 0
        ? widget.appointments.venue['address'] != null
            ? widget.appointments.venue['address']
            : ''
        : '' + '<br>';
    html += "</section>";
    html += "</div></body>";
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.appointments.title),
        centerTitle: true,
        backgroundColor: primaryColor,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Html(
                data: html,
                onLinkTap: (String? url,
                    RenderContext context,
                    Map<String, String> attributes,
                    dom.Element? element) async {
                  await launch(url!);
                }),
          ],
        ),
      ),
    );
  }
}
