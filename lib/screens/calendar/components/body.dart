import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:intl/intl.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:beaconhill/model/calendar_model.dart';
import 'package:beaconhill/screens/calendar/components/detailpage.dart';
import 'package:beaconhill/services/api_manager.dart';
import 'package:beaconhill/utils/constants.dart';

class Body extends StatefulWidget {
  const Body({Key? key}) : super(key: key);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  late Future<CalendarModel> _calendarEvents;
  var model;
  final CalendarController _calendarController = CalendarController();
  late List<Event> _currentMonthAppointments;
  @override
  void initState() {
    super.initState();
    _calendarEvents = ApiManager().getCalendar();
    _currentMonthAppointments = <Event>[];
  }

  Widget calendarData(snapshot) {
    this.model = snapshot.data.events;
    const colorizeTextStyle = TextStyle(
        fontSize: 25,
        color: Colors.blue,
        fontWeight: FontWeight.w500,
        fontStyle: FontStyle.italic);
    var date = DateTime.now();
    return Scaffold(
      body: SafeArea(
          child: Column(
        children: [
          GestureDetector(
            onTap: () => _showDialog(),
            child: Card(
              elevation: 8.0,
              margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              child: Container(
                height: 50,
                width: MediaQuery.of(context).size.width,
                child: Center(
                    child: Text(
                  'Monthly Events',
                  style: colorizeTextStyle,
                )),
              ),
            ),
          ),
          // TextButton(onPressed: _showDialog, child: Text('Monthly Events')),
          Expanded(
            child: SfCalendar(
              controller: _calendarController,
              view: CalendarView.month,
              minDate: DateTime(date.year, date.month - 3, 1),
              maxDate: DateTime(date.year, date.month + 3, 0),
              initialSelectedDate: DateTime.now(),
              cellBorderColor: Colors.transparent,
              monthViewSettings: MonthViewSettings(
                showAgenda: true,
                showTrailingAndLeadingDates: false,
                // navigationDirection: MonthNavigationDirection.horizontal
              ),
              headerStyle: CalendarHeaderStyle(
                  textAlign: TextAlign.center,
                  textStyle: TextStyle(
                      fontSize: 20,
                      fontStyle: FontStyle.normal,
                      letterSpacing: 0,
                      color: primaryColor,
                      fontWeight: FontWeight.w900)),
              showNavigationArrow: true,
              // viewNavigationMode: ViewNavigationMode.none,
              selectionDecoration: BoxDecoration(
                color: Colors.transparent,
                border: Border.all(color: Colors.red, width: 1),
                borderRadius: const BorderRadius.all(Radius.circular(10)),
                shape: BoxShape.rectangle,
              ),
              todayHighlightColor: primaryColor,
              dataSource: MeetingDataSource(snapshot.data.events),
              onViewChanged: calendarViewChanged,
              onTap: calendarTapped,
            ),
          ),
        ],
      )),
    );
  }

  calendarStatic() {
    var date = DateTime.now();
    return SafeArea(
        child: SfCalendar(
      controller: _calendarController,
      view: CalendarView.month,
      minDate: DateTime(date.year, date.month - 3, 1),
      maxDate: DateTime(date.year, date.month + 3, 0),
      initialSelectedDate: DateTime.now(),
      cellBorderColor: Colors.transparent,
      monthViewSettings: MonthViewSettings(
        showAgenda: true,
        showTrailingAndLeadingDates: false,
        // navigationDirection: MonthNavigationDirection.horizontal
      ),
      headerStyle: CalendarHeaderStyle(
          textAlign: TextAlign.center,
          textStyle: TextStyle(
              fontSize: 20,
              fontStyle: FontStyle.normal,
              letterSpacing: 3,
              color: primaryColor,
              fontWeight: FontWeight.w900)),
      showNavigationArrow: true,
      // viewNavigationMode: ViewNavigationMode.none,
      selectionDecoration: BoxDecoration(
        color: Colors.transparent,
        border: Border.all(color: primaryColor, width: 1),
        borderRadius: const BorderRadius.all(Radius.circular(10)),
        shape: BoxShape.rectangle,
      ),
      todayHighlightColor: primaryColor,
    ));
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _calendarEvents,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          if (snapshot.hasData) {
            return this.calendarData(snapshot);
          } else {
            return this.calendarStatic();
          }
        }
        return Center(child: CircularProgressIndicator());
      },
    );
  }

  void calendarTapped(CalendarTapDetails calendarTapDetails) {
    if (calendarTapDetails.targetElement == CalendarElement.appointment) {
      Event appointment = calendarTapDetails.appointments![0];
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => DetailPage(appointment)),
      );
    }
  }

  calendarViewChanged(ViewChangedDetails viewChangedDetails) {
    _currentMonthAppointments = getVisibleAppointments(
      viewChangedDetails
          .visibleDates[viewChangedDetails.visibleDates.length ~/ 2].month,
    );
    SchedulerBinding.instance!.addPostFrameCallback((timeStamp) {
      var midDate = (viewChangedDetails
          .visibleDates[viewChangedDetails.visibleDates.length ~/ 2]);
      if (midDate.month == DateTime.now().month)
        _calendarController.selectedDate = DateTime.now();
      else {
        _calendarController.selectedDate = viewChangedDetails.visibleDates[0];
      }
    });
  }

  List<Event> getVisibleAppointments(int visibleDates) {
    List<Event> visibleAppointment = <Event>[];
    for (int j = 0; j < model.length; j++) {
      if (visibleDates == model[j].startDate.month) {
        visibleAppointment.add(model[j]);
      }
    }
    return visibleAppointment;
  }

  monthlyEvents(events) {
    if (events.isNotEmpty) {
      return Text('Monthly Events ${events.length}');
    } else {
      return Center(child: Text('No Events'));
    }
  }

  _showDialog() async {
    await showDialog(
        context: context,
        builder: (ctx) => Padding(
              padding: const EdgeInsets.all(20.0),
              child: SingleChildScrollView(
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white,
                  ),
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: monthlyEvents(_currentMonthAppointments),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: ListView.builder(
                            shrinkWrap: true,
                            itemCount: _currentMonthAppointments.length,
                            itemBuilder: (BuildContext context, int index) {
                              return GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => DetailPage(
                                            _currentMonthAppointments[index])),
                                  );
                                },
                                child: Padding(
                                  padding: const EdgeInsets.all(5.0),
                                  child: Card(
                                    elevation: 15,
                                    child: Padding(
                                      padding: const EdgeInsets.all(10.0),
                                      child: Row(
                                        children: [
                                          Expanded(
                                            flex: 1,
                                            child: CircleAvatar(
                                              radius: 25,
                                              backgroundColor: primaryColor,
                                              child: Column(
                                                children: [
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            top: 10, bottom: 0),
                                                    child: Text(
                                                        '${DateFormat.d().format(_currentMonthAppointments[index].startDate)}',
                                                        textAlign:
                                                            TextAlign.left,
                                                        style: TextStyle(
                                                            color: Colors.white,
                                                            fontSize: 12)),
                                                  ),
                                                  Text(
                                                      '${DateFormat('EEE').format(_currentMonthAppointments[index].startDate)}',
                                                      textAlign: TextAlign.left,
                                                      style: TextStyle(
                                                          color: Colors.white,
                                                          fontSize: 14))
                                                ],
                                              ),
                                            ),
                                          ),
                                          Expanded(
                                            flex: 2,
                                            child: Column(
                                              children: [
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          top: 2.0,
                                                          bottom: 2.0,
                                                          left: 0.0,
                                                          right: 0.0),
                                                  child: Text(
                                                      '${DateFormat('hh:mm a').format(_currentMonthAppointments[index].startDate)} - ' +
                                                          '${DateFormat('hh:mm a').format(_currentMonthAppointments[index].endDate)}',
                                                      textAlign: TextAlign.left,
                                                      style: TextStyle(
                                                        color: Colors.black,
                                                        fontSize: 13,
                                                      )),
                                                ),
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          top: 2.0,
                                                          bottom: 2.0,
                                                          left: 0.0,
                                                          right: 0.0),
                                                  child: Text(
                                                      _currentMonthAppointments[
                                                              index]
                                                          .title,
                                                      textAlign: TextAlign.left,
                                                      style: TextStyle(
                                                          color: Colors.black,
                                                          fontSize: 15)),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            }),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Align(
                          alignment: Alignment.centerRight,
                          child: OutlinedButton(
                            style: OutlinedButton.styleFrom(
                                side: BorderSide(color: primaryColor)),
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            child: Text('OK',
                                style: TextStyle(color: primaryColor)),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ));
  }
}
