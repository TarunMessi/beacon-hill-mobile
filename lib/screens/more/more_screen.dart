import 'package:beaconhill/screens/more/components/body.dart';
import 'package:flutter/material.dart';
import 'package:beaconhill/utils/constants.dart';

class MoreScreen extends StatelessWidget {
  static String routeName = '/more_screen';

  const MoreScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Body(),
      appBar: AppBar(
        elevation: 0,
        backgroundColor: primaryColor,
        title: Text('More'),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
    );
  }
}
