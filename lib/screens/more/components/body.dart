import 'package:beaconhill/utils/menu_widget.dart';
import 'package:flutter/material.dart';

import 'package:flutter_vector_icons/flutter_vector_icons.dart';

import '../../../utils/constants.dart';

class Body extends StatefulWidget {
  const Body({Key? key}) : super(key: key);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  final _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return Container(
      color: primaryColor,
      child: CustomScrollView(
        controller: _scrollController,
        slivers: [
          SliverFillRemaining(
            hasScrollBody: false,
            child: Column(
              children: [
                Row(
                  children: <Widget>[
                    MenuWidget(
                      iconName: Ionicons.home,
                      displayName: 'Home',
                      routeName: 'home',
                    ),
                    MenuWidget(
                      iconName: Ionicons.newspaper_outline,
                      displayName: 'News',
                      routeName: '/news_screen',
                    ),
                    MenuWidget(
                      iconName: SimpleLineIcons.flag,
                      displayName: 'Alerts',
                      routeName: '/notification_screen',
                    ),
                  ],
                ),
                Row(
                  children: <Widget>[
                    MenuWidget(
                      iconName: FontAwesome.calendar,
                      displayName: 'Calendar',
                      routeName: '/calendar_screen',
                    ),
                    MenuWidget(
                      iconName: FontAwesome.folder_open_o,
                      displayName: 'Documents',
                      routeName: '/documents_screen',
                    ),
                    MenuWidget(
                      iconName: MaterialIcons.content_paste,
                      displayName: 'Newsletter',
                      routeName: '/newsletter_screen',
                    ),
                  ],
                ),
                Row(
                  children: <Widget>[
                    MenuWidget(
                      iconName: Ionicons.location_sharp,
                      displayName: 'Submit A\nRequest',
                      routeName: '/submit_request_screen',
                    ),
                    MenuWidget(
                      iconName: MaterialIcons.payment,
                      displayName: 'Pay\nDues',
                      routeName: 'pay_dues',
                    ),
                    MenuWidget(
                      iconName: MaterialIcons.local_phone,
                      displayName: 'Contact\nNumbers',
                      routeName: '/contact_screen',
                    ),
                  ],
                ),
                Row(
                  children: <Widget>[
                    MenuWidget(
                        iconName: MaterialIcons.settings,
                        displayName: 'Settings',
                        routeName: '/settings_screen'),
                    MenuWidget(
                        iconName: Fontisto.phone,
                        displayName: '',
                        routeName: '',
                        isEmpty: true),
                    MenuWidget(
                        iconName: Fontisto.phone,
                        displayName: '',
                        routeName: '',
                        isEmpty: true),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
