import 'package:beaconhill/model/newsletter_model.dart';
import 'package:beaconhill/screens/newsletter/components/pdf_viewer.dart';
import 'package:flutter/material.dart';
import 'package:grouped_list/grouped_list.dart';
import 'package:beaconhill/services/api_manager.dart';

class Body extends StatefulWidget {
  const Body({Key? key}) : super(key: key);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  late Future<NewsletterModel> _newsletterModel;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _newsletterModel = ApiManager().getNewsletter();
  }

  Widget groupedView(AsyncSnapshot<NewsletterModel> snapshot) {
    if (snapshot.data!.newsletters.isNotEmpty) {
      return GroupedListView<dynamic, String>(
        elements: snapshot.data!.newsletters,
        groupBy: (element) => element.category,
        groupComparator: (value1, value2) => value2.compareTo(value1),
        // itemComparator: (item1, item2) => item1.title.compareTo(item2.title),
        // order: GroupedListOrder.ASC,
        useStickyGroupSeparators: true,
        groupSeparatorBuilder: (String value) => Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            value,
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          ),
        ),
        itemBuilder: (c, element) {
          return GestureDetector(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => PdfViewer(
                            title: element.title,
                            url: element.url,
                          )));
            },
            child: Card(
              elevation: 8.0,
              margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
              child: Container(
                child: ListTile(
                  contentPadding:
                      EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                  title: Text(element.title),
                  trailing: Icon(Icons.arrow_forward),
                ),
              ),
            ),
          );
        },
      );
    } else {
      return Container(
        child: Center(
          child: Text('No more newsletter'),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<NewsletterModel>(
      future: _newsletterModel,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          if (snapshot.hasData) {
            return this.groupedView(snapshot);
          } else {
            return Center(
              child: Text('No more newsletter'),
            );
          }
        }
        return Center(child: CircularProgressIndicator());
      },
    );
  }
}
