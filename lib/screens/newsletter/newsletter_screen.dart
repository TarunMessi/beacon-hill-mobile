import 'package:beaconhill/screens/newsletter/components/body.dart';
import 'package:flutter/material.dart';

class NewsLetterScreen extends StatelessWidget {
  static String routeName = '/newsletter_screen';
  const NewsLetterScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Newsletter'),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: Body(),
    );
  }
}
