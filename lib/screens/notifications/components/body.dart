import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:beaconhill/model/notification_model.dart';
import 'package:beaconhill/model/secure_storage.dart';
import 'package:beaconhill/services/api_manager.dart';
import 'package:intl/intl.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:grouped_list/grouped_list.dart';

class Body extends StatefulWidget {
  const Body({Key? key}) : super(key: key);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  late Future<NotificationModel> _notifications;
  var dateFormat = DateFormat('M/dd');
  var groupFormat = DateFormat('yyyy/MM');
  String STORAGE_KEY_PREVIOUS_COUNT = 'previousCount';
  var _str = SecureStorage();

  @override
  void didChangeDependencies() async {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
    _notifications = ApiManager().getNotification();
    var data = await _notifications;
    var getData = data.data.length;
    _str.writeSecureData(STORAGE_KEY_PREVIOUS_COUNT, json.encode(getData));
  }

  Widget headerTitle(value) {
    DateTime parseDate = new DateFormat("yyyy/MM").parse(value);
    var inputDate = DateTime.parse(parseDate.toString());
    var outputFormat = DateFormat('MMMM yyyy');
    var outputDate = outputFormat.format(inputDate);
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text(
        outputDate,
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
      ),
    );
  }

  Widget groupedList(snapshot) {
    if (snapshot.data!.data.isNotEmpty) {
      return GroupedListView<dynamic, String>(
        elements: snapshot.data!.data,
        groupBy: (element) =>
            groupFormat.format(element.deliveredAt).toString(),
        itemComparator: (item1, item2) => item1.id.compareTo(item2.id),
        order: GroupedListOrder.DESC,
        useStickyGroupSeparators: true,
        groupSeparatorBuilder: (String value) => this.headerTitle(value),
        itemBuilder: (c, element) {
          return Card(
            elevation: 8.0,
            margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
            child: Container(
              child: ListTile(
                  contentPadding:
                      EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                  leading: Wrap(
                    children: [
                      Text(dateFormat.format(element.deliveredAt).toString())
                    ],
                  ),
                  title: Linkify(
                      text: element.message,
                      onOpen: (link) async {
                        if (await canLaunch(link.url)) {
                          await launch(
                            link.url,
                            forceSafariVC: false,
                            forceWebView: false,
                          );
                        } else {
                          throw 'Could not launch $link';
                        }
                      }) //Text(element.message),
                  ),
            ),
          );
        },
      );
    } else {
      return Container(
        child: Center(
          child: Text('No more alerts'),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: FutureBuilder<NotificationModel>(
        future: _notifications,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasData) {
              return this.groupedList(snapshot);
            } else {
              return Center(
                child: Text('No more alerts'),
              );
            }
          }
          return Center(child: CircularProgressIndicator());
        },
      ),
    );
  }

  Widget renderRow(title, date) {
    var outputFormat = DateFormat('MM/dd');
    var outputDate = outputFormat.format(date);
    return Expanded(
      child: Container(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Row(
            children: [
              Icon(
                Icons.date_range_outlined,
                size: 25,
              ),
              SizedBox(
                width: 10,
              ),
              Text(outputDate),
              SizedBox(
                width: 10,
              ),
              Expanded(
                  child: Linkify(
                      text: title,
                      options: LinkifyOptions(humanize: false),
                      onOpen: (link) async {
                        if (await canLaunch(link.url)) {
                          await launch(link.url);
                        } else {
                          throw 'Could not launch $link';
                        }
                      }))
            ],
          ),
        ),
        decoration: BoxDecoration(
            // color: Colors.grey,
            border:
                Border(bottom: BorderSide(color: Colors.black45, width: 0.5))),
      ),
    );
  }
}
