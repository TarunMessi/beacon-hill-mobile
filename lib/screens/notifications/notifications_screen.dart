import 'dart:convert';

import 'package:beaconhill/screens/notifications/components/body.dart';
import 'package:flutter/material.dart';
import 'package:beaconhill/model/secure_storage.dart';

class NotificationScreen extends StatelessWidget {
  static String routeName = '/notification_screen';
  const NotificationScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _str = SecureStorage();
    return Scaffold(
      appBar: AppBar(
        title: Text('Alerts'),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () {
            _str.writeSecureData('fromAlerts', json.encode(true));
            Navigator.of(context).pop();
          },
        ),
      ),
      body: Body(),
    );
  }
}
