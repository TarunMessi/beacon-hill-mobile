import 'package:flutter/material.dart';
import 'package:animated_text_kit/animated_text_kit.dart';

class Submit extends StatelessWidget {
  const Submit({Key? key, this.status}) : super(key: key);
  final status;
  @override
  Widget build(BuildContext context) {
    return responseWidget();
  }

  Widget responseWidget() {
    if (status == 1) {
      return Column(
        children: [
          Container(
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 50, horizontal: 75),
              child: Image.asset(
                'images/success.png',
                fit: BoxFit.contain,
              ),
            ),
          ),
          Padding(
              padding: const EdgeInsets.all(10.0),
              child: AnimatedTextKit(
                displayFullTextOnTap: true,
                totalRepeatCount: 1,
                animatedTexts: [
                  TypewriterAnimatedText('Your request submitted successfully',
                      textStyle:
                          TextStyle(fontSize: 25, fontWeight: FontWeight.w500),
                      textAlign: TextAlign.center)
                ],
              ))
        ],
      );
    } else {
      return Column(
        children: [
          Container(
            child: Image.asset(
              'images/failed.png',
              fit: BoxFit.contain,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Text('Your request submission failed',
                textAlign: TextAlign.center, style: TextStyle(fontSize: 20)),
          )
        ],
      );
    }
  }
}
