import 'package:flutter/foundation.dart';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:beaconhill/model/singleton.dart';
import 'package:beaconhill/utils/constants.dart';

class StoreLocationMap extends StatefulWidget {
  // final Coordinate userLocation;
  const StoreLocationMap({Key? key, this.nextFunction, this.previousFunction})
      : super(key: key);
  final nextFunction;
  final previousFunction;

  @override
  _StoreLocationMapState createState() => _StoreLocationMapState();
}

class _StoreLocationMapState extends State<StoreLocationMap> {
  var s1 = Singleton();
  LatLng latlong = LatLng(39.9498, -90.2104);
  late CameraPosition _cameraPosition;
  late GoogleMapController _controller;
  var location;

  getLocation() async {
    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    location = {'latitude': position.latitude, 'longitude': position.longitude};
    s1.userData['location'] = location;
    if (mounted) {
      setState(() {
        latlong = new LatLng(position.latitude, position.longitude);
        _cameraPosition = CameraPosition(target: latlong, zoom: 15.0);
        if (_controller != null)
          _controller
              .animateCamera(CameraUpdate.newCameraPosition(_cameraPosition));
      });
    }
  }

  void showAlert(BuildContext context) {
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              content: Text(
                "Please select an location!",
                textAlign: TextAlign.center,
              ),
              actions: [
                TextButton(
                  child: Text(
                    "OK",
                    textAlign: TextAlign.center,
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            ));
  }

  void _onCameraMove(CameraPosition position) {
    location = {
      'latitude': position.target.latitude,
      'longitude': position.target.longitude
    };
    s1.userData['location'] = location;
    if (mounted) {
      setState(() {
        latlong = position.target;
      });
    }
  }

  Future getCurrentLocation() async {
    LocationPermission permission = await Geolocator.checkPermission();
    if (permission != PermissionStatus.granted) {
      LocationPermission granted = await Geolocator.requestPermission();
      if (granted != PermissionStatus.granted) {
        getLocation();
      }
      return;
    }
    getLocation();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    var previouslocation = s1.userData['location'];
    if (previouslocation != null) {
      _cameraPosition = CameraPosition(
          target: LatLng(
              previouslocation['latitude'], previouslocation['longitude']),
          zoom: 15.0);
    } else {
      getCurrentLocation();
      _cameraPosition =
          CameraPosition(target: LatLng(39.9498, -90.2104), zoom: 15.0);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Stack(
          children: [
            Stack(
              children: [
                Column(
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height / 2,
                      child: GoogleMap(
                        onCameraMove: _onCameraMove,
                        initialCameraPosition:
                            CameraPosition(target: latlong, zoom: 15.0),
                        mapType: MapType.normal,
                        onMapCreated: (GoogleMapController controller) {
                          _controller = (controller);
                          _controller.animateCamera(
                              CameraUpdate.newCameraPosition(_cameraPosition));
                        },
                        // markers: myMarker(),
                        zoomControlsEnabled: false,
                        myLocationButtonEnabled: Platform.isIOS ? true : false,
                        myLocationEnabled: true,
                      ),
                    ),
                  ],
                ),
                Align(
                  alignment: Alignment.center,
                  heightFactor: 7,
                  child: new Icon(
                    Icons.location_on_sharp,
                    size: 50.0,
                    color: Colors.indigo,
                  ),
                ),
              ],
            ),
            Platform.isAndroid
                ? Positioned(
                    bottom: 10.0,
                    right: 5.0,
                    child: FloatingActionButton(
                      onPressed: getCurrentLocation,
                      backgroundColor: Colors.grey[50],
                      child: Image.asset(
                        'images/mylocation.png',
                        width: 30,
                        height: 30,
                      ),
                    ),
                  )
                : Container(),
          ],
        ),
        Padding(
          padding: EdgeInsets.symmetric(vertical: 25, horizontal: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              previousButton(context),
              nextButton(context),
            ],
          ),
        )
      ],
    );
  }

  Widget nextButton(context) {
    return OutlinedButton(
      style: OutlinedButton.styleFrom(side: BorderSide(color: secondaryColor)),
      onPressed: () {
        var previouslocation = s1.userData['location'];
        if (previouslocation != null) {
          widget.nextFunction(1);
        } else {
          Future.delayed(Duration.zero, () => showAlert(context));
        }
      },
      child: Text('Next', style: TextStyle(color: secondaryColor)),
    );
  }

  /// Returns the previous button.
  Widget previousButton(context) {
    return OutlinedButton(
      style: OutlinedButton.styleFrom(side: BorderSide(color: secondaryColor)),
      onPressed: () {
        var previouslocation = s1.userData['location'];
        if (previouslocation != null) {
          widget.previousFunction(-1);
        } else {
          Future.delayed(Duration.zero, () => showAlert(context));
        }
      },
      child: Text('Prev', style: TextStyle(color: secondaryColor)),
    );
  }
}
