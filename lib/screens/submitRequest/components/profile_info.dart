import 'package:flutter/material.dart';
import 'package:beaconhill/model/singleton.dart';
import 'package:flutter/services.dart';
import 'package:beaconhill/utils/constants.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class ProfileInfo extends StatefulWidget {
  ProfileInfo({Key? key, this.nextFunction}) : super(key: key);
  final nextFunction;

  @override
  _ProfileInfoState createState() => _ProfileInfoState();
}

class _ProfileInfoState extends State<ProfileInfo> {
  final _formKey = GlobalKey<FormState>();
  final _nameFocusNode = FocusNode();
  final _emailFocusNode = FocusNode();
  final _phoneFocusNode = FocusNode();
  var maskFormatter = new MaskTextInputFormatter(
      mask: '(###) ###-####', filter: {"#": RegExp(r'[0-9]')});

  var s1 = Singleton();

  late var _name = {
    'value': s1.userData['name'] == null ? '' : s1.userData['name'],
    'error': ''
  };
  late var _email = {
    'value': s1.userData['email'] == null ? '' : s1.userData['email'],
    'error': ''
  };
  late var _phone = {
    'value': s1.userData['phone'] == null ? '' : s1.userData['phone'],
    'error': ''
  };

  validateName(value) {
    if (value == null || value.isEmpty) {
      setState(() {
        _name['error'] = 'Name is required';
      });
      return _name['error'];
    } else {
      setState(() {
        _name['error'] = '';
        _name['value'] = value;
      });
      return null;
    }
  }

  validateEmail(value) {
    if (value == null || value.isEmpty) {
      setState(() {
        _email['error'] = 'Email is required';
      });
      return _email['error'];
    } else {
      bool emailValid = RegExp(
              r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
          .hasMatch(value);
      if (emailValid) {
        setState(() {
          _email['error'] = '';
          _email['value'] = value;
        });
        return null;
      } else {
        setState(() {
          _email['error'] = 'Email is not valid';
          _email['value'] = value;
        });
        return _email['error'];
      }
    }
  }

  validatePhone(value) {
    if (value == null || value.isEmpty) {
      setState(() {
        _phone['error'] = 'Phone is required';
      });
      return _phone['error'];
    } else if (value.length < 14) {
      setState(() {
        _phone['error'] = 'Phone Number is not valid';
      });
      return _phone['error'];
    } else {
      setState(() {
        _phone['error'] = '';
        _phone['value'] = value;
      });
      return null;
    }
  }

  onChange(name, value) {
    if (name == 'name') {
      validateName(value);
    } else if (name == 'email') {
      validateEmail(value);
    } else if (name == 'phone') {
      validatePhone(value);
    }
  }

  String phoneNumber(num) {
    return num.toString();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: Column(
      children: [
        Container(
          color: formColor,
          child: Row(
            children: [Expanded(flex: 1, child: profileForm())],
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: nextButton(),
            ),
          ],
        )
      ],
    ));
  }

  Widget nextButton() {
    return OutlinedButton(
        style:
            OutlinedButton.styleFrom(side: BorderSide(color: secondaryColor)),
        onPressed: () {
          _formKey.currentState!.validate();
          validateFields();
        },
        child: Text(
          'Next',
          style: TextStyle(color: secondaryColor),
        ));
  }

  validateFields() {
    if (_name['error'] == '' &&
        _email['error'] == '' &&
        _phone['error'] == '') {
      s1.userData['name'] = _name['value'];
      s1.userData['email'] = _email['value'];
      s1.userData['phone'] = _phone['value'];
      widget.nextFunction(1);
    }
  }

  Widget profileForm() {
    return Form(
      key: _formKey,
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          children: [
            TextFormField(
              textCapitalization: TextCapitalization.sentences,
              keyboardType: TextInputType.name,
              focusNode: _nameFocusNode,
              initialValue: _name['value'].toString(),
              decoration: InputDecoration(
                  hintText: 'Name *',
                  errorText: _name['error'].toString() == ''
                      ? null
                      : _name['error'].toString()),
              textInputAction: TextInputAction.next,
              validator: (value) {
                validateName(value);
              },
              onChanged: (value) {
                onChange('name', value);
              },
              onFieldSubmitted: (value) {
                validateName(_name['value']);
                if (_name['error'] == '') {
                  FocusScope.of(context).requestFocus(_emailFocusNode);
                }
              },
            ),
            SizedBox(
              height: 20,
            ),
            TextFormField(
              keyboardType: TextInputType.emailAddress,
              focusNode: _emailFocusNode,
              initialValue: _email['value'].toString(),
              decoration: InputDecoration(
                  hintText: 'Email *',
                  errorText: _email['error'].toString() == ''
                      ? null
                      : _email['error'].toString()),
              textInputAction: TextInputAction.next,
              validator: (value) {
                validateEmail(value);
              },
              onChanged: (value) {
                onChange('email', value);
              },
              onFieldSubmitted: (value) {
                validateEmail(_email['value']);
                if (_email['error'] == '') {
                  FocusScope.of(context).requestFocus(_phoneFocusNode);
                }
              },
            ),
            SizedBox(
              height: 20,
            ),
            TextFormField(
              keyboardType: TextInputType.text,
              inputFormatters: [maskFormatter],
              focusNode: _phoneFocusNode,
              initialValue: phoneNumber(_phone['value'].toString()),
              decoration: InputDecoration(
                  hintText: 'Phone *',
                  errorText: _phone['error'].toString() == ''
                      ? null
                      : _phone['error'].toString()),
              textInputAction: TextInputAction.next,
              validator: (value) {
                validatePhone(value);
              },
              onChanged: (value) {
                onChange('phone', value);
              },
              onFieldSubmitted: (value) {
                _formKey.currentState!.validate(); // Trigger validation
                validateFields();
              },
            ),
          ],
        ),
      ),
    );
  }
}
