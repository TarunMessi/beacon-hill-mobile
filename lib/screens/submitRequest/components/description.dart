import 'package:flutter/material.dart';
import 'package:beaconhill/model/singleton.dart';
import 'package:beaconhill/utils/constants.dart';

class Description extends StatefulWidget {
  Description({Key? key, this.nextFunction, this.previousFunction})
      : super(key: key);
  final nextFunction;
  final previousFunction;

  @override
  _DescriptionState createState() => _DescriptionState();
}

class _DescriptionState extends State<Description> {
  final _descForm = GlobalKey<FormState>();
  var s1 = Singleton();

  late var _description = {
    'value':
        s1.userData['description'] == null ? '' : s1.userData['description'],
    'error': ''
  };
  validateDescription(value) {
    if (value == null || value.isEmpty) {
      setState(() {
        _description['value'] = '';
        _description['error'] = 'Description is required';
      });
      return _description['error'];
    } else {
      setState(() {
        _description['error'] = '';
        _description['value'] = value;
      });
      return null;
    }
  }

  onChange(name, value) {
    if (name == 'description') {
      validateDescription(value);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Container(
          color: formColor,
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: TextFormField(
              textCapitalization: TextCapitalization.sentences,
              maxLines: 5,
              initialValue: _description['value'].toString(),
              decoration: InputDecoration(
                  hintText: 'Description *',
                  errorText: _description['error'].toString() == ''
                      ? null
                      : _description['error'].toString()),
              textInputAction: TextInputAction.newline,
              validator: (value) {
                validateDescription(value);
              },
              onChanged: (value) {
                onChange('description', value);
              },
              onFieldSubmitted: (value) {
                // _descForm.currentState!.validate(); // Trigger validation
                validateDescription(value);
                final error = _description['error'];
                if (error == '') {
                  s1.userData['description'] = _description['value'];
                  widget.nextFunction(1);
                }
              },
            ),
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: previousButton(),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: nextButton(),
            ),
          ],
        )
      ],
    );
    /* return Form(
      key: _descForm,
      child: Column(
        children: [
          Container(
            color: formColor,
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: TextFormField(
                maxLines: 5,
                initialValue: _description['value'].toString(),
                decoration: InputDecoration(
                    hintText: 'Description *',
                    errorText: _description['error'].toString() == ''
                        ? null
                        : _description['error'].toString()),
                textInputAction: TextInputAction.next,
                validator: (value) {
                  validateDescription(value);
                },
                onChanged: (value) {
                  onChange('description', value);
                },
                onFieldSubmitted: (value) {
                  _descForm.currentState!.validate(); // Trigger validation
                  final error = _description['error'].toString();
                  if (error.isEmpty) {
                    widget.nextFunction(1);
                  }
                },
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 25),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  previousButton(),
                  nextButton(),
                ],
              ),
            ),
          )
        ],
      ),
    );*/
  }

  Widget nextButton() {
    return OutlinedButton(
      style: OutlinedButton.styleFrom(side: BorderSide(color: secondaryColor)),
      onPressed: () {
        validateDescription(_description['value']);
        // _descForm.currentState!.validate();
        if (_description['error'] == '') {
          s1.userData['description'] = _description['value'];
          widget.nextFunction(1);
        }
      },
      child: Text('Next', style: TextStyle(color: secondaryColor)),
    );
  }

  /// Returns the previous button.
  Widget previousButton() {
    return OutlinedButton(
      style: OutlinedButton.styleFrom(side: BorderSide(color: secondaryColor)),
      onPressed: () {
        // _descForm.currentState!.validate();
        if (_description['error'] == '') {
          s1.userData['description'] = _description['value'];
          widget.previousFunction(-1);
        }
      },
      child: Text('Prev', style: TextStyle(color: secondaryColor)),
    );
  }
}
