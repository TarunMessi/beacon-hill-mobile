import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io' as Io;
// import 'package:uuid/uuid.dart';
// import 'package:mime_type/mime_type.dart';
import 'package:beaconhill/model/singleton.dart';
import 'package:beaconhill/utils/constants.dart';

enum ImageSourceType { gallery, camera }

class PhotoPicker extends StatefulWidget {
  const PhotoPicker({Key? key, this.submitFunction, this.previousFunction})
      : super(key: key);
  final submitFunction;
  final previousFunction;

  @override
  _PhotoPickerState createState() => _PhotoPickerState();
}

class _PhotoPickerState extends State<PhotoPicker> {
  final ImagePicker _picker = ImagePicker();
  List<dynamic> images = Singleton().userData['images'] != null
      ? Singleton().userData['images']
      : [];
  List imagesData = Singleton().userData['imagesData'] != null
      ? Singleton().userData['imagesData']
      : [];
  int selectedIndex = -1;
  var s1 = Singleton();

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
      children: [
        newDesign(),
        Padding(
          padding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              previousButton(),
              nextButton(context),
            ],
          ),
        )
      ],
    ) //HomePage(),
        );
  }

  _imgFromCamera() async {
    final XFile? image = await _picker.pickImage(source: ImageSource.camera);
    if (image != null) {
      setImage(image);
    }
  }

  _imgFromGallery() async {
    final XFile? image = await _picker.pickImage(source: ImageSource.gallery);
    if (image != null) {
      setImage(image);
    }
  }

  void setImage(item) async {
    final bytes = Io.File(item.path).readAsBytesSync();
    String img64 = base64Encode(bytes);

    Io.File image =
        new Io.File(item.path); // Or any other way to get a File instance.

    setState(() {
      images.add({'selected': false, 'path': image.path.toString()});
      imagesData.add(img64);
    });
    s1.userData['imagesData'] = imagesData;
    s1.userData['images'] = images;
  }

  Widget newDesign() {
    return Column(
      children: [
        Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.width,
          child: ListView.builder(
              itemCount: images.length,
              itemBuilder: (context, int index) {
                return displayImage(images[index], index);
              }),
        ),
        SizedBox(
          height: 50,
          width: double.infinity,
          child: Visibility(
            child: actionSheet(),
            visible: images.length <= photoLimit - 1,
          ),
        )
      ],
    );
  }

  Widget displayImage(item, int index) {
    return GestureDetector(
      onTap: () {
        var selectedIndex = images[index];
        selectedIndex['selected'] = false;
        var copyImage = images;
        copyImage.removeAt(index);
        copyImage.insert(index, selectedIndex);
        setState(() {
          images = copyImage;
        });
      },
      onLongPress: () {
        var selectedIndex = images[index];
        selectedIndex['selected'] = true;
        var copyImage = images;
        copyImage.removeAt(index);
        copyImage.insert(index, selectedIndex);
        setState(() {
          images = copyImage;
        });
      },
      child: Container(
        decoration: new BoxDecoration(color: Colors.white),
        // height: 240,
        child: Stack(
          children: <Widget>[
            Center(
              child: Image.file(
                Io.File(item['path']),
                fit: BoxFit.fill,
                height: MediaQuery.of(context).size.width / 2,
                width: MediaQuery.of(context).size.width / 2,
              ),
            ),
            deleteIcon(item['selected'], index),
          ],
        ),
      ),
    );
    // Image.file(
    //   Io.File(item),
    //   height: MediaQuery.of(context).size.width / 2,
    //   width: MediaQuery.of(context).size.width / 2,
    // );
  }

  Widget deleteIcon(value, index) {
    print(value);
    return Visibility(
      visible: value,
      child: Positioned(
          top: 15,
          right: 15, //give the values according to your requirement
          child: IconButton(
              onPressed: () {
                var copyImage = images;
                copyImage.removeAt(index);
                var copyData = imagesData;
                copyData.removeAt(index);
                setState(() {
                  images = copyImage;
                  imagesData = copyData;
                });
              },
              icon: Icon(
                Icons.delete,
                color: Colors.redAccent,
                size: 30,
              ))),
    );
  }

  Widget actionSheet() {
    return CupertinoButton(
      alignment: Alignment.bottomCenter,
      color: Colors.green,
      onPressed: () {
        showCupertinoModalPopup<void>(
          context: context,
          builder: (BuildContext context) => CupertinoActionSheet(
            actions: <CupertinoActionSheetAction>[
              CupertinoActionSheetAction(
                child: const Text('Camera'),
                onPressed: () {
                  _imgFromCamera();
                  Navigator.of(context).pop();
                },
              ),
              CupertinoActionSheetAction(
                child: const Text('Choose from Album'),
                onPressed: () {
                  _imgFromGallery();
                  Navigator.of(context).pop();
                },
              ),
              CupertinoActionSheetAction(
                isDefaultAction: true,
                child: const Text(
                  'Cancel',
                  style: TextStyle(color: Colors.red),
                ),
                onPressed: () {
                  Navigator.pop(context, 'Cancel');
                },
              )
            ],
          ),
        );
      },
      child: const Text(
        'Add Photos',
        style: TextStyle(color: Colors.white, fontSize: 20),
      ),
    );
  }

  Widget nextButton(cont) {
    return OutlinedButton(
      style: OutlinedButton.styleFrom(side: BorderSide(color: secondaryColor)),
      onPressed: () {
        s1.userData['imagesData'] = imagesData;
        s1.userData['images'] = images;
        widget.submitFunction(1, cont);
      },
      child: Text('Submit', style: TextStyle(color: secondaryColor)),
    );
  }

  /// Returns the previous button.
  Widget previousButton() {
    return OutlinedButton(
      style: OutlinedButton.styleFrom(side: BorderSide(color: secondaryColor)),
      onPressed: () {
        s1.userData['imagesData'] = imagesData;
        s1.userData['images'] = images;
        widget.previousFunction(-1);
      },
      child: Text('Prev', style: TextStyle(color: secondaryColor)),
    );
  }
}
