import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';
import 'package:im_stepper/stepper.dart';
import 'package:beaconhill/model/singleton.dart';
import 'package:beaconhill/screens/submitRequest/components/description.dart';
import 'package:beaconhill/screens/submitRequest/components/location.dart';
import 'package:beaconhill/screens/submitRequest/components/photo.dart';
import 'package:beaconhill/screens/submitRequest/components/profile_info.dart';
import 'package:beaconhill/screens/submitRequest/components/submit.dart';
import 'package:beaconhill/services/api_manager.dart';
import 'package:beaconhill/utils/constants.dart';
import 'package:beaconhill/model/submit_request_model.dart';
import 'package:beaconhill/utils/loader.dart';
import 'package:connectivity/connectivity.dart';

class Body extends StatefulWidget {
  const Body({Key? key}) : super(key: key);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  int activeStep = 0;

  int upperBound = 4;
  int status = 0;
  late SubmitARequestModel _request;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          IconStepper(
            enableNextPreviousButtons: false,
            steppingEnabled: false,
            activeStepColor: primaryColor,
            icons: [
              Icon(
                Icons.supervised_user_circle,
                color: Colors.white,
              ),
              Icon(MaterialIcons.content_paste, color: Colors.white),
              Icon(MaterialIcons.location_on, color: Colors.white),
              Icon(MaterialIcons.photo_camera, color: Colors.white),
              Icon(MaterialIcons.beenhere, color: Colors.white),
            ],
            activeStep: activeStep,
            onStepReached: (index) {
              setState(() {
                activeStep = index;
              });
            },
          ),
          header(),
          childContainer(),
        ],
      ),
    );
  }

  void nextAction(value) {
    setState(() {
      activeStep++;
    });
  }

  void prevAction(value) {
    if (activeStep >= 0) {
      setState(() {
        activeStep--;
      });
    }
  }

  void submitAction(value, cont) async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      LoadingGauge loader = LoadingGauge();
      loader.showLoader(context);
      _request = await ApiManager().submitRequest();
      loader.hideLoader();
      Singleton().userData = new Map();
      if (_request.status == 1) {
        setState(() {
          status = 1;
          activeStep++;
        });
      } else {
        setState(() {
          activeStep++;
        });
      }
    } else {
      showAlertDialog(cont);
    }
  }

  /// Returns the header wrapping the header text.
  Widget header() {
    return Container(
      decoration: BoxDecoration(
        color: primaryColor,
        borderRadius: BorderRadius.circular(0),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              headerText(),
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w500,
                fontSize: 20,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget childContainer() {
    switch (activeStep) {
      case 1:
        return Description(
            nextFunction: nextAction, previousFunction: prevAction);

      case 2:
        return StoreLocationMap(
            nextFunction: nextAction, previousFunction: prevAction);

      case 3:
        return PhotoPicker(
            submitFunction: submitAction, previousFunction: prevAction);

      case 4:
        return Submit(
          status: status,
        );

      default:
        return ProfileInfo(nextFunction: nextAction);
    }
  }

  // Returns the header text based on the activeStep.
  String headerText() {
    switch (activeStep) {
      case 1:
        return 'Description';

      case 2:
        return 'Location';

      case 3:
        return 'Photos';

      case 4:
        return 'Submit';

      default:
        return 'Contact Info';
    }
  }

  showAlertDialog(BuildContext context) {
    Widget okButton = TextButton(
      child: Text("OK"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );

    AlertDialog alert = AlertDialog(
      title: Text('No Internet Connection'),
      content: Text(
        'You are not connected to the internet. Make sure Wi-Fi is on, Airplane Mode is off and try again.',
        textAlign: TextAlign.center,
      ),
      actions: [okButton],
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
