import 'package:flutter/material.dart';
import 'package:beaconhill/model/singleton.dart';
import 'package:beaconhill/screens/submitRequest/components/body.dart';

class SubmitARequestScreen extends StatelessWidget {
  const SubmitARequestScreen({Key? key}) : super(key: key);
  static String routeName = '/submit_request_screen';

  @override
  Widget build(BuildContext context) {
    void getBack(context) {
      Singleton().userData = new Map();
      Navigator.of(context).pop();
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('Submit A Request'),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => getBack(context),
        ),
      ),
      body: Body(),
    );
  }
}
