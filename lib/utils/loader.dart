import 'package:flutter/material.dart';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart';

import 'constants.dart';

class LoadingGauge {
  void showLoader(BuildContext context) {
    Loader.show(
      context,
      progressIndicator: CircularProgressIndicator(
        backgroundColor: warmGreyColor,
      ),
      themeData: Theme.of(context).copyWith(accentColor: darkSlateBlueColor),
    );
  }

  void hideLoader() {
    Loader.hide();
  }
}
