import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:beaconhill/utils/constants.dart';
import 'package:badges/badges.dart';

class MenuWidget extends StatelessWidget {
  final IconData iconName;
  final String displayName;
  final bool border;
  final String routeName;
  final bool isEmpty;
  final int badgeNo;
  const MenuWidget(
      {Key? key,
      required this.iconName,
      required this.displayName,
      this.border = false,
      required this.routeName,
      this.isEmpty = false,
      this.badgeNo = 0})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        decoration: borderWidget(),
        width: 10,
        height: MediaQuery.of(context).size.height / 6,
        child: TextButton(
          onPressed: () {
            if (routeName != '') {
              if (routeName == 'home') {
                Navigator.of(context).pop();
              } else if (routeName == 'pay_dues') {
                _launchURL('https://myaccount.pmpbiz.com/');
              } else {
                Navigator.pushNamed(context, routeName);
              }
            }
          },
          child: columnWidget(),
        ),
        // color: Colors.green,
      ),
    );
  }

  borderWidget() {
    if (border) {
      return BoxDecoration(
          border: Border.all(color: Colors.white), color: primaryColor);
    } else {
      return BoxDecoration(color: primaryColor);
    }
  }

  void _launchURL(url) async => await canLaunch(url)
      ? await launch(
          url,
          forceSafariVC: false,
          forceWebView: false,
        )
      : throw 'Could not launch $url';

  columnWidget() {
    if (isEmpty) {
      return Container();
    } else if (badgeNo > 0) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Badge(
            badgeContent: Text(
              badgeNo.toString(),
              style: TextStyle(color: Colors.white),
            ),
            child: Icon(
              iconName,
              size: 30,
              color: iconColor,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 5.0),
            child: Text(
              displayName,
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.white, fontSize: 13),
            ),
          ),
        ],
      );
    } else {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
            iconName,
            size: 30,
            color: iconColor,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 5.0),
            child: Text(
              displayName,
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.white, fontSize: 13),
            ),
          ),
        ],
      );
    }
  }
}
