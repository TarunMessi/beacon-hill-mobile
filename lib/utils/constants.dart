import 'package:flutter/material.dart';

const Color primaryColor = Color(0xff153807);
const Color iconColor = Color.fromRGBO(255, 255, 255, 1);
const String oneSignalAppID = '9d93b43e-b8cd-4fff-bf50-16b7f1f4c03a';
const String apiURL = 'http://44.195.158.76';
const String mokoURL = 'https://notifications.mokoapp.com/api';
const String apiKey = 'J7CYSHQW4M4RW9PQJ4YH5XQK57PK';
const Color secondaryColor =
    Color.fromRGBO(222, 168, 73, 1); //Color(0xffdea849);
const photoLimit = 3;
const Color formColor = Color.fromRGBO(225, 222, 217, 1); //Color(0xffe1ded9);
const Color warmGreyColor = Color.fromRGBO(141, 137, 137, 1);
const Color darkSlateBlueColor = Color.fromRGBO(30, 69, 110, 1);
