// To parse this JSON data, do
//
//     final submitARequestModel = submitARequestModelFromJson(jsonString);

import 'dart:convert';

SubmitARequestModel submitARequestModelFromJson(String str) =>
    SubmitARequestModel.fromJson(json.decode(str));

String submitARequestModelToJson(SubmitARequestModel data) =>
    json.encode(data.toJson());

class SubmitARequestModel {
  SubmitARequestModel({
    required this.status,
    required this.messageId,
    required this.message,
  });

  int status;
  String messageId;
  String message;

  factory SubmitARequestModel.fromJson(Map<String, dynamic> json) =>
      SubmitARequestModel(
        status: json["status"],
        messageId: json["messageId"],
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "messageId": messageId,
        "message": message,
      };
}
