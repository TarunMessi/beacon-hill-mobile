import 'dart:convert';

CarouselModel carouselModelFromJson(String str) =>
    CarouselModel.fromJson(json.decode(str));

String carouselModelToJson(CarouselModel data) => json.encode(data.toJson());

class CarouselModel {
  CarouselModel({
    required this.carouselImages,
  });

  List<String> carouselImages;

  factory CarouselModel.fromJson(Map<String, dynamic> json) => CarouselModel(
        carouselImages:
            List<String>.from(json["carousel_images"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "carousel_images": List<dynamic>.from(carouselImages.map((x) => x)),
      };
}
