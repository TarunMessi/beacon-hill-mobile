// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

ContactModel welcomeFromJson(String str) => ContactModel.fromJson(json.decode(str));

String welcomeToJson(ContactModel data) => json.encode(data.toJson());

class ContactModel {
  ContactModel({
    required this.contactNumbers,
  });

  List<ContactNumber> contactNumbers;

  factory ContactModel.fromJson(Map<String, dynamic> json) => ContactModel(
    contactNumbers: List<ContactNumber>.from(json["contact_numbers"].map((x) => ContactNumber.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "contact_numbers": List<dynamic>.from(contactNumbers.map((x) => x.toJson())),
  };
}

class ContactNumber {
  ContactNumber({
    required this.title,
    required this.name,
    this.number,
    this.email,
  });

  String title;
  String name;
  dynamic number;
  dynamic email;

  factory ContactNumber.fromJson(Map<String, dynamic> json) => ContactNumber(
    title: json["title"],
    name: json["name"],
    number: json["number"],
    email: json["email"],
  );

  Map<String, dynamic> toJson() => {
    "title": title,
    "name": name,
    "number": number,
    "email": email,
  };
}
