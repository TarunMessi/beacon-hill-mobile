// To parse this JSON data, do
//
//     final calendarModel = calendarModelFromJson(jsonString);

// import 'package:meta/meta.dart';
import 'dart:convert';

import 'package:syncfusion_flutter_calendar/calendar.dart';

CalendarModel welcomeFromJson(String str) =>
    CalendarModel.fromJson(json.decode(str));

String welcomeToJson(CalendarModel data) => json.encode(data.toJson());

class CalendarModel {
  CalendarModel({
    required this.events,
  });

  List<Event> events;

  factory CalendarModel.fromJson(Map<String, dynamic> json) => CalendarModel(
        events: List<Event>.from(json["events"].map((x) => Event.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "events": List<dynamic>.from(events.map((x) => x.toJson())),
      };
}

class Event {
  Event({
    required this.id,
    required this.globalId,
    required this.globalIdLineage,
    required this.author,
    required this.status,
    required this.date,
    required this.dateUtc,
    required this.modified,
    required this.modifiedUtc,
    required this.url,
    required this.restUrl,
    required this.title,
    required this.description,
    required this.excerpt,
    required this.slug,
    required this.image,
    required this.allDay,
    required this.startDate,
    required this.startDateDetails,
    required this.endDate,
    required this.endDateDetails,
    required this.utcStartDate,
    required this.utcStartDateDetails,
    required this.utcEndDate,
    required this.utcEndDateDetails,
    required this.timezone,
    required this.timezoneAbbr,
    required this.cost,
    required this.costDetails,
    required this.website,
    required this.showMap,
    required this.showMapLink,
    required this.hideFromListings,
    required this.sticky,
    required this.featured,
    required this.categories,
    required this.tags,
    required this.venue,
    required this.organizer,
  });

  int id;
  String globalId;
  List<String> globalIdLineage;
  String author;
  String status;
  DateTime date;
  DateTime dateUtc;
  DateTime modified;
  DateTime modifiedUtc;
  String url;
  String restUrl;
  String title;
  String description;
  String excerpt;
  String slug;
  bool image;
  bool allDay;
  DateTime startDate;
  DateDetails startDateDetails;
  DateTime endDate;
  DateDetails endDateDetails;
  DateTime utcStartDate;
  DateDetails utcStartDateDetails;
  DateTime utcEndDate;
  DateDetails utcEndDateDetails;
  String timezone;
  String timezoneAbbr;
  String cost;
  CostDetails costDetails;
  String website;
  bool showMap;
  bool showMapLink;
  bool hideFromListings;
  bool sticky;
  bool featured;
  List<Category> categories;
  List<Category> tags;
  dynamic venue;
  List<Organizer> organizer;

  factory Event.fromJson(Map<String, dynamic> json) => Event(
        id: json["id"],
        globalId: json["global_id"],
        globalIdLineage:
            List<String>.from(json["global_id_lineage"].map((x) => x)),
        author: json["author"],
        status: json["status"],
        date: DateTime.parse(json["date"]),
        dateUtc: DateTime.parse(json["date_utc"]),
        modified: DateTime.parse(json["modified"]),
        modifiedUtc: DateTime.parse(json["modified_utc"]),
        url: json["url"],
        restUrl: json["rest_url"],
        title: json["title"],
        description: json["description"],
        excerpt: json["excerpt"],
        slug: json["slug"],
        image: json["image"],
        allDay: json["all_day"],
        startDate: DateTime.parse(json["start_date"]),
        startDateDetails: DateDetails.fromJson(json["start_date_details"]),
        endDate: DateTime.parse(json["end_date"]),
        endDateDetails: DateDetails.fromJson(json["end_date_details"]),
        utcStartDate: DateTime.parse(json["utc_start_date"]),
        utcStartDateDetails:
            DateDetails.fromJson(json["utc_start_date_details"]),
        utcEndDate: DateTime.parse(json["utc_end_date"]),
        utcEndDateDetails: DateDetails.fromJson(json["utc_end_date_details"]),
        timezone: json["timezone"],
        timezoneAbbr: json["timezone_abbr"],
        cost: json["cost"],
        costDetails: CostDetails.fromJson(json["cost_details"]),
        website: json["website"],
        showMap: json["show_map"],
        showMapLink: json["show_map_link"],
        hideFromListings: json["hide_from_listings"],
        sticky: json["sticky"],
        featured: json["featured"],
        categories: List<Category>.from(
            json["categories"].map((x) => Category.fromJson(x))),
        tags:
            List<Category>.from(json["tags"].map((x) => Category.fromJson(x))),
        venue: json["venue"],
        organizer: List<Organizer>.from(
            json["organizer"].map((x) => Organizer.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "global_id": globalId,
        "global_id_lineage": List<dynamic>.from(globalIdLineage.map((x) => x)),
        "author": author,
        "status": status,
        "date": date.toIso8601String(),
        "date_utc": dateUtc.toIso8601String(),
        "modified": modified.toIso8601String(),
        "modified_utc": modifiedUtc.toIso8601String(),
        "url": url,
        "rest_url": restUrl,
        "title": title,
        "description": description,
        "excerpt": excerpt,
        "slug": slug,
        "image": image,
        "all_day": allDay,
        "start_date": startDate.toIso8601String(),
        "start_date_details": startDateDetails.toJson(),
        "end_date": endDate.toIso8601String(),
        "end_date_details": endDateDetails.toJson(),
        "utc_start_date": utcStartDate.toIso8601String(),
        "utc_start_date_details": utcStartDateDetails.toJson(),
        "utc_end_date": utcEndDate.toIso8601String(),
        "utc_end_date_details": utcEndDateDetails.toJson(),
        "timezone": timezone,
        "timezone_abbr": timezoneAbbr,
        "cost": cost,
        "cost_details": costDetails.toJson(),
        "website": website,
        "show_map": showMap,
        "show_map_link": showMapLink,
        "hide_from_listings": hideFromListings,
        "sticky": sticky,
        "featured": featured,
        "categories": List<dynamic>.from(categories.map((x) => x.toJson())),
        "tags": List<dynamic>.from(tags.map((x) => x.toJson())),
        "venue": venue,
        "organizer": List<dynamic>.from(organizer.map((x) => x.toJson())),
      };
}

class Category {
  Category({
    required this.name,
    required this.slug,
    required this.termGroup,
    required this.termTaxonomyId,
    required this.taxonomy,
    required this.description,
    required this.parent,
    required this.count,
    required this.filter,
    required this.id,
    required this.urls,
  });

  String name;
  String slug;
  int termGroup;
  int termTaxonomyId;
  String taxonomy;
  String description;
  int parent;
  int count;
  String filter;
  int id;
  Urls urls;

  factory Category.fromJson(Map<String, dynamic> json) => Category(
        name: json["name"],
        slug: json["slug"],
        termGroup: json["term_group"],
        termTaxonomyId: json["term_taxonomy_id"],
        taxonomy: json["taxonomy"],
        description: json["description"],
        parent: json["parent"],
        count: json["count"],
        filter: json["filter"],
        id: json["id"],
        urls: Urls.fromJson(json["urls"]),
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "slug": slug,
        "term_group": termGroup,
        "term_taxonomy_id": termTaxonomyId,
        "taxonomy": taxonomy,
        "description": description,
        "parent": parent,
        "count": count,
        "filter": filter,
        "id": id,
        "urls": urls.toJson(),
      };
}

class Urls {
  Urls({
    required this.self,
    required this.collection,
  });

  String self;
  String collection;

  factory Urls.fromJson(Map<String, dynamic> json) => Urls(
        self: json["self"],
        collection: json["collection"],
      );

  Map<String, dynamic> toJson() => {
        "self": self,
        "collection": collection,
      };
}

class CostDetails {
  CostDetails({
    required this.currencySymbol,
    required this.currencyPosition,
    required this.values,
  });

  String currencySymbol;
  String currencyPosition;
  List<dynamic> values;

  factory CostDetails.fromJson(Map<String, dynamic> json) => CostDetails(
        currencySymbol: json["currency_symbol"],
        currencyPosition: json["currency_position"],
        values: List<dynamic>.from(json["values"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "currency_symbol": currencySymbol,
        "currency_position": currencyPosition,
        "values": List<dynamic>.from(values.map((x) => x)),
      };
}

class DateDetails {
  DateDetails({
    required this.year,
    required this.month,
    required this.day,
    required this.hour,
    required this.minutes,
    required this.seconds,
  });

  String year;
  String month;
  String day;
  String hour;
  String minutes;
  String seconds;

  factory DateDetails.fromJson(Map<String, dynamic> json) => DateDetails(
        year: json["year"],
        month: json["month"],
        day: json["day"],
        hour: json["hour"],
        minutes: json["minutes"],
        seconds: json["seconds"],
      );

  Map<String, dynamic> toJson() => {
        "year": year,
        "month": month,
        "day": day,
        "hour": hour,
        "minutes": minutes,
        "seconds": seconds,
      };
}

class Organizer {
  Organizer({
    required this.id,
    required this.author,
    required this.status,
    required this.date,
    required this.dateUtc,
    required this.modified,
    required this.modifiedUtc,
    required this.url,
    required this.organizer,
    required this.slug,
    required this.globalId,
    required this.globalIdLineage,
  });

  int id;
  String author;
  String status;
  DateTime date;
  DateTime dateUtc;
  DateTime modified;
  DateTime modifiedUtc;
  String url;
  String organizer;
  String slug;
  String globalId;
  List<String> globalIdLineage;

  factory Organizer.fromJson(Map<String, dynamic> json) => Organizer(
        id: json["id"],
        author: json["author"],
        status: json["status"],
        date: DateTime.parse(json["date"]),
        dateUtc: DateTime.parse(json["date_utc"]),
        modified: DateTime.parse(json["modified"]),
        modifiedUtc: DateTime.parse(json["modified_utc"]),
        url: json["url"],
        organizer: json["organizer"],
        slug: json["slug"],
        globalId: json["global_id"],
        globalIdLineage:
            List<String>.from(json["global_id_lineage"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "author": author,
        "status": status,
        "date": date.toIso8601String(),
        "date_utc": dateUtc.toIso8601String(),
        "modified": modified.toIso8601String(),
        "modified_utc": modifiedUtc.toIso8601String(),
        "url": url,
        "organizer": organizer,
        "slug": slug,
        "global_id": globalId,
        "global_id_lineage": List<dynamic>.from(globalIdLineage.map((x) => x)),
      };
}

class VenueClass {
  VenueClass({
    required this.id,
    required this.author,
    required this.status,
    required this.date,
    required this.dateUtc,
    required this.modified,
    required this.modifiedUtc,
    required this.url,
    required this.venue,
    required this.slug,
    required this.address,
    required this.city,
    required this.country,
    required this.province,
    required this.state,
    required this.zip,
    required this.stateprovince,
    required this.showMap,
    required this.showMapLink,
    required this.globalId,
    required this.globalIdLineage,
  });

  int id;
  String author;
  String status;
  DateTime date;
  DateTime dateUtc;
  DateTime modified;
  DateTime modifiedUtc;
  String url;
  String venue;
  String slug;
  String address;
  String city;
  String country;
  String province;
  String state;
  String zip;
  String stateprovince;
  bool showMap;
  bool showMapLink;
  String globalId;
  List<String> globalIdLineage;

  factory VenueClass.fromJson(Map<String, dynamic> json) => VenueClass(
        id: json["id"],
        author: json["author"],
        status: json["status"],
        date: DateTime.parse(json["date"]),
        dateUtc: DateTime.parse(json["date_utc"]),
        modified: DateTime.parse(json["modified"]),
        modifiedUtc: DateTime.parse(json["modified_utc"]),
        url: json["url"],
        venue: json["venue"],
        slug: json["slug"],
        address: json["address"],
        city: json["city"],
        country: json["country"],
        province: json["province"],
        state: json["state"],
        zip: json["zip"],
        stateprovince: json["stateprovince"],
        showMap: json["show_map"],
        showMapLink: json["show_map_link"],
        globalId: json["global_id"],
        globalIdLineage:
            List<String>.from(json["global_id_lineage"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "author": author,
        "status": status,
        "date": date.toIso8601String(),
        "date_utc": dateUtc.toIso8601String(),
        "modified": modified.toIso8601String(),
        "modified_utc": modifiedUtc.toIso8601String(),
        "url": url,
        "venue": venue,
        "slug": slug,
        "address": address,
        "city": city,
        "country": country,
        "province": province,
        "state": state,
        "zip": zip,
        "stateprovince": stateprovince,
        "show_map": showMap,
        "show_map_link": showMapLink,
        "global_id": globalId,
        "global_id_lineage": List<dynamic>.from(globalIdLineage.map((x) => x)),
      };
}

class MeetingDataSource extends CalendarDataSource {
  MeetingDataSource(List<Event> source) {
    appointments = source;
  }
  @override
  DateTime getStartTime(int index) {
    return appointments![index].startDate;
  }

  @override
  DateTime getEndTime(int index) {
    return appointments![index].endDate;
  }

  @override
  bool isAllDay(int index) {
    return appointments![index].allDay;
  }

  @override
  String getSubject(int index) {
    return appointments![index].title;
  }
}
