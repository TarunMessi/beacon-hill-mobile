// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

NotificationModel welcomeFromJson(String str) =>
    NotificationModel.fromJson(json.decode(str));

String welcomeToJson(NotificationModel data) => json.encode(data.toJson());

class NotificationModel {
  NotificationModel({
    required this.data,
  });

  List<Datum> data;

  factory NotificationModel.fromJson(Map<String, dynamic> json) =>
      NotificationModel(
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum(
      {required this.id,
      required this.accountId,
      required this.message,
      required this.sendAt,
      required this.deliveredAt,
      required this.createdAt,
      required this.updatedAt});

  int id;
  int accountId;
  String message;
  DateTime sendAt;
  DateTime deliveredAt;
  DateTime createdAt;
  DateTime updatedAt;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        accountId: json["account_id"],
        message: json["message"],
        sendAt: DateTime.parse(json["send_at"]),
        deliveredAt: DateTime.parse(json["delivered_at"]),
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "account_id": accountId,
        "message": message,
        "send_at": sendAt.toIso8601String(),
        "delivered_at": deliveredAt.toIso8601String(),
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
      };
}
