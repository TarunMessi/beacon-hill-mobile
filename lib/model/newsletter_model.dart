// To parse this JSON data, do
//
//     final newsletterModel = newsletterModelFromJson(jsonString);

import 'dart:convert';

NewsletterModel newsletterModelFromJson(String str) =>
    NewsletterModel.fromJson(json.decode(str));

String newsletterModelToJson(NewsletterModel data) =>
    json.encode(data.toJson());

class NewsletterModel {
  NewsletterModel({
    required this.newsletters,
  });

  List<Newsletter> newsletters;

  factory NewsletterModel.fromJson(Map<String, dynamic> json) =>
      NewsletterModel(
        newsletters: List<Newsletter>.from(
            json["newsletters"].map((x) => Newsletter.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "newsletters": List<dynamic>.from(newsletters.map((x) => x.toJson())),
      };
}

class Newsletter {
  Newsletter({
    required this.category,
    required this.id,
    required this.title,
    required this.url,
  });

  String category;
  int id;
  String title;
  String url;

  factory Newsletter.fromJson(Map<String, dynamic> json) => Newsletter(
        category: json["category"],
        id: json["id"],
        title: json["title"],
        url: json["url"],
      );

  Map<String, dynamic> toJson() => {
        "category": category,
        "id": id,
        "title": title,
        "url": url,
      };
}
