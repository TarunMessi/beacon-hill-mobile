// To parse this JSON data, do
//
//     final documentsModel = documentsModelFromJson(jsonString);

import 'dart:convert';

DocumentsModel documentsModelFromJson(String str) =>
    DocumentsModel.fromJson(json.decode(str));

String documentsModelToJson(DocumentsModel data) => json.encode(data.toJson());

class DocumentsModel {
  DocumentsModel({
    required this.documents,
  });

  List<Document> documents;

  factory DocumentsModel.fromJson(Map<String, dynamic> json) => DocumentsModel(
        documents: List<Document>.from(
            json["documents"].map((x) => Document.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "documents": List<dynamic>.from(documents.map((x) => x.toJson())),
      };
}

class Document {
  Document({
    required this.category,
    required this.id,
    required this.title,
    required this.url,
  });

  String category;
  int id;
  String title;
  String url;

  factory Document.fromJson(Map<String, dynamic> json) => Document(
        category: json["category"],
        id: json["id"],
        title: json["title"],
        url: json["url"],
      );

  Map<String, dynamic> toJson() => {
        "category": category,
        "id": id,
        "title": title,
        "url": url,
      };
}
