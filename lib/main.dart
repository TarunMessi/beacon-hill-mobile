import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:beaconhill/screens/home/home_screen.dart';
import 'package:beaconhill/services/api_manager.dart';
import 'package:beaconhill/utils/constants.dart' as constant;
import 'package:beaconhill/routes.dart';
import 'package:flutter/material.dart';
import 'dart:async';
// import 'package:beaconhill/screens/home/home_screen.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'dart:io' show Platform;
import 'package:flutter/services.dart';

void main() async {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: constant.primaryColor, // status bar color
  ));
  WidgetsFlutterBinding.ensureInitialized();
  await FlutterDownloader.initialize(debug: false);
  runApp(App());
}

class App extends StatefulWidget {
  const App({Key? key}) : super(key: key);

  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          appBarTheme: AppBarTheme(color: constant.primaryColor),
          primaryTextTheme:
              TextTheme(headline6: TextStyle(color: Colors.white)),
          primaryColor: constant.primaryColor),
      routes: routes,
      initialRoute: HomeScreen.routeName,
    );
  }

  Future<void> initPlatformState() async {
    if (!mounted) return;
    await OneSignal.shared.setAppId(constant.oneSignalAppID);
    ApiManager().syncNotificationTags();
    if (Platform.isIOS) {
      OneSignal.shared
          .promptUserForPushNotificationPermission()
          .then((accepted) {});
    }
    OneSignal.shared
        .setNotificationOpenedHandler((OSNotificationOpenedResult result) {
      // Will be called whenever a notification is opened/button pressed.
    });
    OneSignal.shared.setNotificationWillShowInForegroundHandler(
        (OSNotificationReceivedEvent event) {
      event.complete(null);
    });
  }
}
